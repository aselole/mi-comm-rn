const colors = {
  buttonAction: '#1CAADE',
  buttonActionTitle: '#eeeeee',
  buttonDisabled: '#ededed',
  buttonDisabledTitle: '#525252',

  theme: '#F62459',
  theme2: 'rgb(66, 165, 245)',
  themeLight: '#cccccc',
  themeDark: '#C93756',
  themeAccent: '#aaa',
  background: '#595959',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  primary: '#42a5f5',

  /*

    Referensi: BeoUI style

  */
  // black and white theme
	main: '#FFFFFF', // '#1CB5B4',
	toolbar: 'rgba(255, 255, 255, 0.95)',
	toolbarTint: "#1CB5B4",
	text: '#333333',
	menuCategory: "rgba(0, 0, 0, 0.8)",
	menuCategoryActive: "rgba(255, 255, 255, 0.1)",
	menuCategoryActiveText: "rgba(255, 255, 255, 1)",
	menuCategoryActiveBorder: "rgba(255, 255, 255, 0.1)",
	menuItem: "#999999",

	// // color theme
	// main: '#1CB5B4', //'#7f8c8d', // '#c0392b', // '#8e44ad', // '#34495e', //'#1CB5B4',
	// toolbarTint: "#1CB5B4",
	// toolbar: '#FFFFFF',
	// text: '#FFFFFF',
	//
	// menuCategory: "rgba(0, 0, 0, 0.4)",
	// menuCategoryActive: "rgba(255, 255, 255, 0.1)",
	// menuCategoryActiveText: "rgba(255, 255, 255, 1)",
	// menuCategoryActiveBorder: "rgba(255, 255, 255, 0.1)",
	// menuItem: "#ccc",

	spin: '#333333',
	time: '#aaaaaa',
	title: '#333333',
	colors: [
		'rgba(58, 75, 133, 0.6)',
		'rgba(188, 59, 36, 0.6)',
		'rgba(57, 174, 84, 0.6)',
		'rgba(188, 59, 36, 0.6)',
		'rgba(141, 114, 91, 0.6)',
		'rgba(128, 140, 141, 0.6)'
	]
}

export default colors
