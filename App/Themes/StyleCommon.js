import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Fonts from './Fonts'
const {width, height, scale} = Dimensions.get("window"),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh);
const isIphoneX = (height * scale) > 2000;

export default StyleSheet.create({
  detailContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  detailSlide: {
    flexGrow: 3,
    marginTop: 6,
    marginHorizontal: 6,
    height: 400,
    flexDirection: 'row'
  },
  detailTitleContainer: {
    flexGrow: 1,
    marginTop: -60,
    height: 120,
    paddingHorizontal: 15,
    backgroundColor: 'rgba(116,47,138,0.7)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  detailTitle: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: Fonts.type.base,
  },
  detailSubTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#011',
    marginTop: 20,
    marginLeft: 16,
    fontFamily: Fonts.type.base,
  },
  detailContentContainer: {
    flex: 1,
    flexGrow: 5,
  },
  detailContentDate: {
    fontSize: 12,
    alignSelf: 'center',
    fontStyle: 'italic',
    fontFamily: Fonts.type.base,
    color: '#474747',
    marginBottom: 10,
    padding: 12,
  },
  detailContent: {
    fontSize: 16,
    textAlign: 'auto',
    flexWrap: 'wrap',
  },
  arrowContainer: {
    flexGrow: 1,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  arrow: {
    flexGrow: 1,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonBottomContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  buttonDoubleBottomContainer: {
    flexDirection: 'row',
    paddingBottom: 16,
  },
  buttonBottom: {
    flex: 1,
    alignSelf: 'stretch',
    borderRadius: 0,
    fontFamily: Fonts.type.base,
  },
  toolbarRightContainer: {
    flex:1,
    flexDirection: 'row',
    marginRight: 12,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
});
