import React, { Component } from 'react'
import { Platform, Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import {
  TextInputIcon, ButtonBox, NotifDialog, ListFilter,
  ImageLoading, Text
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import FlexImage from 'react-native-flex-image'
import { convertError } from '../../Lib/AppCustomUtils'
import Swiper from 'react-native-swiper'
import Parallax from '../../Lib/react-native-parallax'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import css from '../../Themes/Style'

class EventList extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Promo',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      page_index: 0,
      error_info_global: '',
      error_info_member: '',
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getEventGlobal(this.props.token)
    this.props.getEventMember(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info_global, nextProps.error_info_global) && !_.isNull(nextProps.error_info_global)) {
      this.setState({ error_info_global: convertError(nextProps.error_info_global) })
    }
    if(!_.isEqual(this.props.error_info_member, nextProps.error_info_member) && !_.isNull(nextProps.error_info_member)) {
      this.setState({ error_info_member: convertError(nextProps.error_info_member) })
    }
  }

  _onListClicked(item) {
    this.props.onListClicked(item.promo_id)
    this.props.navigation.navigate('EventDetail')
  }

  _onPageChanged(index) {
    this.setState({ page_index: index })
  }

  _normalizeParallaxData(input, key) {
    let output = _.cloneDeep(input)
    output.map((item, i) => {
      item[key] = AppConfig.promoPhotoURL+item[key]
    })
    return output
  }

  render () {
    return (
      <View style={{flex:1, backgroundColor:'#fff'}}>
        <View style={{flexDirection:'row'}}>
          <TouchableOpacity
            onPress={() => this._onPageChanged(0)}
            disabled={Platform.OS === 'ios' ? false : true}
            style={styles.tabContainer}>
            <Text style={styles.tabTitle}>
              Global
            </Text>
            { this.state.page_index === 0 && <View style={styles.tabBorder} /> }
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this._onPageChanged(1)}
            disabled={Platform.OS === 'ios' ? false : true}
            style={styles.tabContainer}>
            <Text style={styles.tabTitle}>
              Member
            </Text>
            { this.state.page_index === 1 && <View style={styles.tabBorder} /> }
          </TouchableOpacity>
        </View>
        <Swiper
          ref={(ref) => this._swiper = ref}
          loop={false}
          showsPagination={false}
          index={this.state.page_index}
          onIndexChanged={(index) => this._onPageChanged(index)}>
          <View style={{flexGrow:1}}>
            <ListFilter
              data={this.props.data_global}
              errorInfo={this.state.error_info_global}
              errorTitle={this.props.error_title_global}
              isFetching={this.props.is_fetching_global}
              keys={'promo_id'}
              loadingType='pacman'
              style={{flex:1}}
              type='parallax'
              parallaxBaseImageUri={AppConfig.promoPhotoURL}
              parallaxImageKey='featured_img'
              parallaxTitleKey='judul'
              onItemClicked={(item) => this._onListClicked(item)}
            />
          </View>
          <View style={{flexGrow:1}}>
            <ListFilter
              data={this.props.data_member}
              errorInfo={this.state.error_info_member}
              errorTitle={this.props.error_title_member}
              isFetching={this.props.is_fetching_member}
              keys={'promo_id'}
              loadingType='pacman'
              style={{flex:1}}
              type='parallax'
              parallaxBaseImageUri={AppConfig.promoPhotoURL}
              parallaxImageKey='featured_img'
              parallaxTitleKey='judul'
              onItemClicked={(item) => this._onListClicked(item)}
            />
          </View>
        </Swiper>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    error_info_global: state.list.error_info_array[0],
    error_info_member: state.list.error_info_array[1],
    error_title_global: state.list.error_title_array[0],
    error_title_member: state.list.error_title_array[1],
    data_global: state.list.data_array[0],
    data_member: state.list.data_array[1],
    is_fetching_global: state.list.is_fetching_array[0],
    is_fetching_member: state.list.is_fetching_array[1],
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getEventGlobal: (token) => dispatch(ListActions.getEventGlobal(token)),
    getEventMember: (token) => dispatch(ListActions.getEventMember(token)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    resetList: () => dispatch(ListActions.resetList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventList)
