import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clear,
    justifyContent: 'center',
  },
  arrowContainer: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  arrow: {
    width: 30,
    height:30
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  dialogContainer: {
    flexGrow: 0.2,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
  dialogTitle: {
    flexGrow: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  dialogInputContainer: {
    flexGrow: 0.2,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  dialogInput: {
    flexGrow: 0.1,
    fontSize: 14,
    backgroundColor: 'transparent',
  },
  dialogInputBorder: {
    height: 1,
    backgroundColor: 'red',
  },
  dialogButtonContainer: {
    flexGrow: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  dialogButton: {
    flexGrow: 1,
    flexBasis: (deviceWidth/2)-100,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabContainer: {
    flex:1,
    flexDirection:'column',
    paddingVertical:5
  },
  tabTitle: {
    alignSelf:'center',
    fontSize:18,
    marginBottom:5
  },
  tabBorder: {
    height:5,
    backgroundColor:'grey'
  }
})
