import React, { Component } from 'react'
import { Picker, KeyboardAvoidingView, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import {
  TextInputIcon, NotifDialog, LoadingOverlay, ButtonIcon,
  ImageLoading, ConfirmDialog, ModalLoading, ImageFitLoading,
  TextJustify
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { parseHTML } from '../../Lib/HTMLUtils'
import { convertError } from '../../Lib/AppCustomUtils'
import { getReadableDate } from '../../Lib/DateUtils'
import Modal from 'react-native-modal'
import Slideshow from 'react-native-slideshow'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class EventDetail extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Promo',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      data: null,
      ket: null,
      input_kode_visible: false,
      is_claimed: false,
      title_button: '',
      kode: '',
      status: true,
      is_empty_kode: false,
      is_submit_kode: false,
      image: null
    }
  }

  componentDidMount() {
    this.props.getDetail(this.props.token, this.props.id)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this.setState({
        data: nextProps.data,
        image: nextProps.data.featured_img,
        ket: nextProps.data.body,
        is_claimed: _.isEmpty(nextProps.data.is_claimed) ? true : false,
        title_button: _.isEmpty(nextProps.data.is_claimed) ? "PROMO SUDAH DI CLAIM" : "AMBIL PROMO",
      })
    }
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
    if(!_.isEqual(this.props.data_promo, nextProps.data_promo)) {
      if(!_.isNull(nextProps.data_promo)) {
        this.setState({
          is_claimed: true,
          title_button: "PROMO SUDAH DI CLAIM",
        })
      }
    }
  }

  // tampilkan dialog input kode
  _onStartInputKode() {
    this.setState({ input_kode_visible: true, is_empty_kode: false, kode: '' })
  }

  // user jadi kirim kode voucher
  _onSubmitKode() {
    if(_.isEmpty(this.state.kode)) {
      this.setState({ is_empty_kode: true })
      return
    }
    this.setState({ is_submit_kode: true, input_kode_visible: false, is_empty_kode: false })
  }

  // user batal kirim kode voucher
  _onCancelKode() {
    this.setState({ is_submit_kode: false, input_kode_visible: false })
  }

  // tampilkan loading stlh dialog input kode sembunyi
  _onHideInputKode() {
    if(this.state.is_submit_kode)
     this.props.takePromo(this.props.token, this.state.data.promo_id, this.state.kode)
  }

  // render modal untuk input kode promo
  _renderInputKode = () => {
    return (
      <KeyboardAvoidingView style={styles.dialogContainer}>
        <View style={styles.dialogTitle}>
          <Text style={[ stylesTheme.mediumBold, styles.buttonTitle ]}>
            Masukkan Kode Voucher
          </Text>
        </View>
        <View style={styles.dialogInputContainer}>
          <TextInput
            onChangeText={(kode) => this.setState({kode})}
            value={this.state.kode}
            style={styles.dialogInput}
            underlineColorAndroid='rgba(0,0,0,0)' />
          <View style={styles.dialogInputBorder} />
          {this.state.is_empty_kode &&
            <Text style={{color:'red'}}>
              Kode Voucher harus diisi
            </Text>
          }
        </View>
        <View style={styles.dialogButtonContainer}>
          <TouchableOpacity
            style={styles.dialogButton}
            onPress={() => this._onSubmitKode()}>
            <Text
              style={[stylesTheme.mediumBold, stylesTheme.whiteColor, styles.buttonTitle ]}>
              OK
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.dialogButton, {backgroundColor:'grey'}]}
            onPress={() => this._onCancelKode()}>
            <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, styles.buttonTitle ]}>
              CANCEL
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    )
  }

  render () {
    if(this.state.is_fetching) {
      return (
        <View style={stylesCommon.detailContainer}>
          <DotIndicator color={Colors.theme} count={4} size={10} style={{marginTop: 20}} />
        </View>
      )
    } else {
      return (
        <View style={{flexGrow:1}}>

          <View style={{flexGrow:1}}>
            <View style={{flex:10}}>
              <ScrollView
                style={stylesCommon.detailContainer}
                showsVerticalScrollIndicator={false}>
                <View style={stylesCommon.detailSlide}>
                  <ImageLoading
                    name={this.state.image}
                    urlDefault={AppConfig.defaultPhotoURL}
                    urlBase={AppConfig.promoPhotoURL}
                    path={this.state.image} />
                </View>
                <View style={stylesCommon.detailTitleContainer}>
                  <Text
                    numberOfLines={2}
                    style={stylesCommon.detailTitle}>{this.state.data.judul}</Text>
                </View>
                <View style={stylesCommon.detailContentContainer}>
                  <Text style={stylesCommon.detailSubTitle}>
                    Berlaku sampai {getReadableDate(this.state.data.valid_until, 'id', 'DD MMM YYYY')}
                  </Text>
                  <TextJustify
                    text={this.state.ket}
                    styleContainer={{marginTop: 20}} />
                </View>
              </ScrollView>
            </View>

            <ButtonIcon
              title={this.state.title_button}
              onPress={() => this._onStartInputKode()}
              isDisabled={this.state.is_claimed}
              bgColor={Colors.red}
              styleContainer={stylesCommon.buttonBottomContainer}
              style={stylesCommon.buttonBottom}
              bgColor={Colors.buttonAction}
              loadingBgColor={Colors.buttonDisabled}
              titleColor={Colors.buttonActionTitle}
              loadingTitleColor={Colors.buttonDisabledTitle}
            />
          </View>

          <Modal
            isVisible={this.state.input_kode_visible}
            backdropColor='gray'
            backdropOpacity={0.5}
            animationInTiming={250}
            onModalHide={() => this._onHideInputKode()}>
            {this._renderInputKode()}
          </Modal>

          <ModalLoading
            title='Cek Kode Promo'
            isFetching={this.props.is_fetching_promo}
            responseOk={this.props.data_promo}
            responseFail={this.props.error_info_promo}
          />
        </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data: state.list.detail,
    id: state.list.id,
    is_fetching: state.list.is_fetching,
    data_promo: state.transaction.response,
    is_fetching_promo: state.transaction.is_fetching,
    error_info_promo: state.transaction.error_info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getEventDetail(token, id)),
    takePromo: (token, promo_id, kode) => dispatch(TransactionActions.takePromo(token, promo_id, kode)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetail)
