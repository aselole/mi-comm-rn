import React, { Component } from 'react'
import {
  Picker, ScrollView, Image, View,
  TouchableHighlight, TouchableOpacity,
  TextInput, AsyncStorage, WebView, Share
} from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ImageFullscreen, ButtonBox, ImageLoading,
  Text, ImageFitLoading, TextJustify, ToolbarButton
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { parseHTML } from '../../Lib/HTMLUtils'
import Slideshow from 'react-native-slideshow'
import { getReadableDate } from '../../Lib/DateUtils'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'

class BeritaDetail extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Berita',
      headerStyle: { elevation:0 },
      headerRight:
      <View style={stylesCommon.toolbarRightContainer}>
        <ToolbarButton
          image={Images.share}
          onPress={() => params.onBagiArtikel()}
          styleContainer={{flex:0.44, backgroundColor:'transparent'}}
        />
      </View>
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      data: null,
      body: null,
      image: null,
      isFullImageShown: false,
      imageSelectedIndex: 0,
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onBagiArtikel: this._bagiArtikel
    })
    this.props.getDetail(this.props.token, this.props.id)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this.setState({
        data: nextProps.data,
        body: nextProps.data.body,
        image: this._decodeImage(nextProps.data),
        ImageFullScreen: this._decodeFullScreenImage(nextProps.data),
      })
    }
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
  }

  _decodeImage(input) {
    let output = []
    input.image_news.map((item) => {
      output.push({ url: AppConfig.beritaPhotoURL+item.image })
    })
    return output
  }

  _decodeFullScreenImage(input) {
    let output = []
    input.image_news.map((item) => {
      output.push(AppConfig.beritaPhotoURL+item.image)
    })
    return output
  }

  _onImagePressed(image, index) {
    this.setState({
      imageSelectedIndex: index,
      isFullImageShown: true
    })
  }

  _onToggleImageFullScreen() {
    this.setState({
      isFullImageShown: false
    })
  }

  // bagikan artikel ke app lain
  _bagiArtikel = (item) => {
    Share.share(
      { message: this.state.data.url },
      { dialogTitle: 'Bagikan dengan' }
    )
  }

  render () {
    if(this.state.is_fetching) {
      return (
        <View style={stylesCommon.detailContainer}>
          <DotIndicator color={Colors.theme} count={4} size={10} style={{marginTop: 20}} />
        </View>
      )
    } else if(!this.state.is_fetching && !_.isNull(this.state.body) && !_.isNull(this.state.image)){
      return (
        <View style={{flex:1}}>
          <ScrollView
            style={stylesCommon.detailContainer}
            showsVerticalScrollIndicator={false}>
            <View style={stylesCommon.detailSlide}>
              <Slideshow
                dataSource={this.state.image}
                height={400}
                indicatorSize={0}
                onPress={(image, index) => this._onImagePressed(image, index)}
                arrowSize={28}
                arrowLeft={
                  <View style={stylesCommon.arrowContainer}>
                    <ImageFitLoading urlDefault={Images.left} style={stylesCommon.arrow}/>
                  </View>
                }
                arrowRight={
                  <View style={stylesCommon.arrowContainer}>
                    <ImageFitLoading urlDefault={Images.right} style={stylesCommon.arrow}/>
                  </View>
                }/>
            </View>
            <View style={stylesCommon.detailTitleContainer}>
              <Text
                numberOfLines={2}
                style={stylesCommon.detailTitle}>{this.state.data.judul}</Text>
            </View>
            <View style={stylesCommon.detailContentContainer}>
              <TextJustify
                text={this.state.body}
                styleContainer={{marginTop: 25}} />
              <Text style={stylesCommon.detailContentDate}>
                Ditulis pada tanggal {getReadableDate(this.state.data.created_at, 'id', 'DD MMM YYYY')}
              </Text>
            </View>
            <ImageFullscreen
              data={this.state.ImageFullScreen}
              isShown={this.state.isFullImageShown}
              currentImage={this.state.imageSelectedIndex}
              onToggle={this._onToggleImageFullScreen.bind(this)}
            />
          </ScrollView>
      </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data: state.list.detail,
    id: state.list.id,
    is_fetching: state.list.is_fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getBeritaDetail(token, id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BeritaDetail)
