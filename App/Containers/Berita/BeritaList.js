import React, { Component } from 'react'
import {
  Picker, ScrollView, Share, Image, View,
  TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage
} from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import {
  TextInputIcon, ButtonBox, NotifDialog, ListFilter,
  ImageLoading, ImageFitLoading, Text, SwipedView
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import FlexImage from 'react-native-flex-image'
import { convertError } from '../../Lib/AppCustomUtils'
import { parseHTML } from '../../Lib/HTMLUtils'
import { getIntervalDateToday } from '../../Lib/DateUtils'
import { replaceSpecialChar } from '../../Lib/ManipulationUtils'
import Swipeable from 'react-native-swipeable'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import css from '../../Themes/Style'

class BeritaList extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Berita',
      headerStyle: css.noShadow,
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      error_info: '',
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getList(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ error_info: convertError(nextProps.error_info) })
    }
  }

  _onListClicked(item) {
    this.props.onListClicked(item.id)
    this.props.navigation.navigate('BeritaDetail')
  }

  // bagikan artikel ke app lain
  _bagiArtikel(item, index) {
    Share.share(
      { message: AppConfig.beritaShareURL+item.id+'-'+replaceSpecialChar(item.judul, '-').toLowerCase().replace(/-+$/, "") },
      { dialogTitle: 'Bagikan dengan' }
    )
  }

  _renderList = ({ item, index }) => {
    return (
      <Swipeable
        key={item.id}
        rightContent={<SwipedView icon={Images.share} isLeft={true}/>}
        leftContent={<SwipedView icon={Images.share} isLeft={false}/>}
        rightActionActivationDistance={100}
        leftActionActivationDistance={100}
        onRightActionRelease={() => this._bagiArtikel(item, index)}
        onLeftActionRelease={() => this._bagiArtikel(item, index)}>
        <TouchableOpacity
          key={item.id}
          style={styles.listContainer}
          onPress={() => this._onListClicked(item)}>
          <View style={styles.listImage}>
            <ImageLoading
              name={item.image_news}
              urlDefault={AppConfig.defaultPhotoURL}
              urlBase={AppConfig.beritaPhotoURL}
              path={item.image_news}
              style={{flexGrow:1}}/>
          </View>
          <View style={styles.listContent}>
            <View style={{ flex:3, backgroundColor:'transparent' }}>
              <Text style={styles.listTitle} numberOfLines={2}>
                {item.judul}
              </Text>
              <Text style={styles.listDate} numberOfLines={1}>
                {getIntervalDateToday(item.published_at, 'DD MMM')}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </Swipeable>
  );
}

  render () {
    return (
      <View style={{flexGrow:1, backgroundColor:'#fff'}}>
        <ListFilter
          data={this.props.data}
          errorInfo={this.state.error_info}
          errorTitle={this.props.error_title}
          isFetching={this.props.is_fetching}
          renderRow={this._renderList.bind(this)}
          loadingType='pacman'
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    error_title: state.list.error_title,
    error_info: state.list.error_info,
    data: state.list.data,
    is_fetching: state.list.is_fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (token) => dispatch(ListActions.getBerita(token)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    resetList: () => dispatch(ListActions.resetList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BeritaList)
