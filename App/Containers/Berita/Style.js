import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clear,
    justifyContent: 'center',
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  listContainer: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: vh * 18,
  },
  listContent: {
    flex: 2.2,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    marginHorizontal: 5,
    marginVertical: 8
  },
  listImage: {
    flex: 1,
    flexGrow: 1,
    marginTop: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    borderRadius: 2
  },
  listTitle: {
    fontSize: 14,
    marginRight: 8,
    color: '#333',
    fontWeight: '400'
  },
  listDate: {
    color: "#999",
    fontSize: 11,
    marginTop: 6
  },
})
