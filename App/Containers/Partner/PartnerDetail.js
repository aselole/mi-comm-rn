import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ButtonBox, ImageLoading, TextJustify 
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { parseHTML } from '../../Lib/HTMLUtils'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'

class PartnerDetail extends Component {

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      image: null,
      data: null,
      ket: null
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this.setState({
        data: nextProps.data.partner,
        image: nextProps.data.partner.logo,
        ket: nextProps.data.partner.ket,
      })
    }
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
  }

  componentDidMount() {
    this.props.getDetail(this.props.token, this.props.id)
  }

  render () {
    if(this.state.is_fetching) {
      return (
        <View style={stylesCommon.detailContainer}>
          <DotIndicator color={Colors.theme} count={4} size={10} style={{marginTop: 20}} />
        </View>
      )
    } else if(!this.state.is_fetching && this.state.data){
      return (
        <ScrollView
          style={stylesCommon.detailContainer}
          showsVerticalScrollIndicator={false}>
          <View style={stylesCommon.detailSlide}>
            <ImageLoading
              name={this.state.image}
              urlDefault={AppConfig.defaultPhotoURL}
              urlBase={AppConfig.partnerPhotoURL}
              path={this.state.image} />
          </View>
          <View style={stylesCommon.detailTitleContainer}>
            <Text
              numberOfLines={2}
              style={stylesCommon.detailTitle}>{this.state.data.nama}</Text>
          </View>
          <View style={stylesCommon.detailContentContainer}>
            <Text style={stylesCommon.detailSubTitle}>
              {this.state.data.alamat}
            </Text>
            <Text style={stylesCommon.detailSubTitle}>
              Telepon: {this.state.data.telepon1}
            </Text>
            <TextJustify
              text={this.state.ket}
              styleContainer={{marginTop: 20}} />
          </View>
        </ScrollView>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data: state.list.detail,
    id: state.list.id,
    is_fetching: state.list.is_fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getPartnerDetail(token, id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PartnerDetail)
