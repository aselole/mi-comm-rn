import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 15,
    backgroundColor: '#fff'
  },
  logo: {
    width: deviceWidth / 1.5,
    alignSelf: 'center',
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  textInput: {

  },
  textRegisterTitle: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    marginTop: 15,
  },
  textRegister: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  textForgotPass: {
    textAlign: 'right', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  dropdown: {
    height:50,
    width:null,
    borderColor: 'lightgray',
    borderRadius: 2,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  listContainer: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: vh * 18,
  },
  listImage: {
    flex: 1,
    flexGrow: 1,
    marginTop: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    borderRadius: 2
  },
  listContent: {
    flex: 2.2,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    marginHorizontal: 5,
    marginVertical: 8
  },
  listTitle: {
    fontSize: 14,
    marginRight: 8,
    color: '#333',
    fontWeight: '400',
    fontFamily: Fonts.type.base,
  },
  listDate: {
    color: "#999",
    fontSize: 11,
    marginTop: 6
  },
})
