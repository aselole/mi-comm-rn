import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { TextInputIcon, ButtonBox, NotifDialog, ListFilter, ImageLoading } from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import FlexImage from 'react-native-flex-image'
import { convertError } from '../../Lib/AppCustomUtils'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import css from '../../Themes/Style'

class DealList extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Partnership Deals',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      error_info: '',
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getList(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ error_info: convertError(nextProps.error_info) })
    }
  }

  _onListClicked(item) {
    this.props.onListClicked(item.id)
    this.props.navigation.navigate('DealDetail')
  }

  _renderList = ({ item, index }) => (
    <TouchableOpacity
      key={item.id}
      style={[ styles.listContainer, {backgroundColor: index % 2 === 0 ? '#ebebeb' : '#fff'} ]}
      onPress={() => this._onListClicked(item)}>
      <View style={styles.listImage}>
        <ImageLoading
          name={item.logo_merchant}
          urlDefault={AppConfig.defaultPhotoURL}
          urlBase={AppConfig.dealPhotoURL}
          path={item.logo_merchant}
          style={{flexGrow:1}} />
      </View>
      <View style={styles.listContent}>
        <Text style={styles.listTitle} numberOfLines={2}>
          {_.upperFirst(_.toLower(item.nama))}
        </Text>
      </View>
    </TouchableOpacity>
  )

  render () {
    return (
      <View style={styles.container}>
        <ListFilter
          data={this.props.data}
          errorInfo={this.state.error_info}
          errorTitle={this.props.error_title}
          isFetching={this.props.is_fetching}
          renderRow={this._renderList.bind(this)}
          loadingType='pacman'
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
    data: state.list.data,
    is_fetching: state.list.is_fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (token) => dispatch(ListActions.getDeals(token)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    resetList: () => dispatch(ListActions.resetList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DealList)
