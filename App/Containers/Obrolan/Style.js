import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 0,
    height: deviceHeight - 200,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    margin: 0,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: -5
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  modalSubcontainer: {
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  modalScrollContainer: {
    flex:0,
    height: deviceHeight - 300
  },
  arrowContainer: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  arrow: {
    width: 30,
    height:30
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: 15,
    backgroundColor: 'white',
  },
  inputContentContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  dialogTitleContainer: {
    flexGrow: 0.05,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  inputTextContainer: {
    flexGrow: 0.8,
    flexDirection: 'column',
    backgroundColor: 'transparent',
  },
  inputText1Container: {
    flexGrow: 1,
    backgroundColor: 'orange',
  },
  inputText2Container: {
    flexGrow: 3,
    backgroundColor: 'yellow',
  },
  inputText3Container: {
    flexGrow: 1,
    backgroundColor: 'orange',
  },
  dialogInputBorder: {
    height: 1,
    backgroundColor: 'red',
  },
  inputButtonContainer: {
    flexGrow: 0.2,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  dialogButton: {
    flexGrow: 1,
    flexBasis: (deviceWidth/2)-100,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
  },
  chatContainer: {
    backgroundColor:'#D3FFFF',
    flexDirection: 'row',
    alignSelf:'flex-end',
    padding: 10,
    borderRadius: 10,
    marginRight: 20,
    marginBottom: 10,
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.2
  },
  chatBodyMessage: {
    fontSize: 16,
    marginRight: 25,
    alignSelf:'flex-start',
    marginBottom:3,
  },
  chatBodyDate: {
    fontSize: 12,
    alignSelf:'flex-end',
    color:'gray'
  },
  chatBodyCheck: {
    marginLeft: 5,
    width:15,
    height:15,
    alignSelf:'flex-end'
  },
  chatReplyContainer: {
    backgroundColor:'white',
    flexDirection: 'row',
    alignSelf:'flex-start',
    padding: 10,
    borderRadius: 10,
    marginLeft: 20,
    marginBottom: 10,
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.2
  },
  chatReplyBodyMessage: {
    fontSize: 16,
    marginRight: 25,
    alignSelf:'flex-start',
    marginBottom:3,
  },
  chatReplyBodyDate: {
    fontSize: 12,
    alignSelf:'flex-end',
    color:'gray'
  },
  chatReplyBodyCheck: {
    marginLeft: 5,
    width:15,
    height:15,
    alignSelf:'flex-end'
  },
  chatHeaderContainer: {
    backgroundColor:'white',
    alignSelf:'center',
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
  chatRoomHeader: {
    flexGrow: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 5,
    marginBottom: 5,
    elevation: 5,
    shadowOffset: {
      width: 2,
      height: 1
    },
    shadowRadius: 4,
    shadowOpacity: 0.4
  },
  chatRoomHeaderImageContainer: {
    flexGrow: 1,
    alignSelf: 'center',
  },
  chatRoomHeaderBody: {
    flexGrow: 3,
    justifyContent:'center',
    marginLeft: 10,
  },
  chatInputContainer: {
    flexGrow: 0,
    flexDirection: 'row',
    alignItems: 'stretch',
    backgroundColor: 'white',
  },
  input: {
    "height": 40,
    "borderColor": "#ddd",
    "borderWidth": 1,
    "fontSize": 14,
    "borderRadius": 4,
    "padding": 4,
    paddingLeft: 12,
    "marginHorizontal": 20,
    "marginBottom": 8,
    "color": "#333",
    fontFamily: Fonts.type.base,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingBottom: 16,
  },
  tabContainer: {
    flex:1,
    flexDirection:'column',
    paddingVertical:5
  },
  tabTitle: {
    alignSelf:'center',
    fontSize:18,
    marginBottom:5
  },
  listContainer: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: vh * 18,
  },
  listContent: {
    flex: 2.5,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    marginHorizontal: 5,
    marginVertical: 8
  },
  listAction: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 8,
    backgroundColor: 'transparent'
  },
  listImage: {
    flex: 1,
    flexGrow: 1,
    marginTop: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    borderRadius: 2
  },
  listTitle: {
    fontSize: 14,
    marginRight: 8,
    color: '#333',
    fontWeight: '400'
  },
  listSubtitle: {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '300',
  },
  listDate: {
    color: "#999",
    fontSize: 11,
    marginTop: 6
  },
  "detailBlock": {
    "alignItems": "center",
    justifyContent: 'center',
    "backgroundColor": "rgba(255,255,255,0.8)",
    //backgroundColor: 'yellow',
    padding: 20,
    width: width,
    height: 100,
    marginTop: -100,
  },
  "detailName": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 18,
    "paddingTop": 8
  },
  detailDescBlock: {
    padding: 20,
    justifyContent:'center',
    alignItems:'center'
  },
  "detailDesc": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 13,
    "textAlign": "center",
  },
  "detailPrice": {
    "fontSize": 18,
    "width": 100,
    "textAlign": "center"
  },
  "detailFullPrice": {
    "fontSize": 15,
  },
  detailImage: {
    width: width,
    height: 400,
  },
})
