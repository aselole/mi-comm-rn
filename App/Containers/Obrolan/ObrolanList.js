import React, { Component } from 'react'
import { Platform, KeyboardAvoidingView, Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import {
  NotifDialog, ListFilter, ImageFitLoading, ImageLoading,
  ConfirmDialog, ModalLoading, ButtonIcon, ImagePicker, Text,
  SwipedView
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import { getReadableDate, getCurrentDate } from '../../Lib/DateUtils'
import Modal from 'react-native-modal'
import Swiper from 'react-native-swiper'
import Swipeable from 'react-native-swipeable'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class ObrolanList extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Obrolan',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      data_comm: null,
      data_private: null,
      error_info_comm: null,
      page_index: 0,
      is_show_delete_confirm: 0,
      issue_selected: null,
      issue_selected_index: -1,
      is_show_loading: false,
      is_show_input_issue: false,
      issue_judul: '',
      issue_isi: '',
      issue_foto: null,
      is_confirmed_submit_issue: false,
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getListComm(this.props.token)
    this.props.getListPrivate(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info_comm, nextProps.error_info_comm) && !_.isNil(nextProps.error_info_comm)) {
      this.setState({ error_info_comm: convertError(nextProps.error_info_comm) })
    }
    if(!_.isEqual(this.props.error_info_private, nextProps.error_info_private) && !_.isNil(nextProps.error_info_private)) {
      this.setState({ error_info_private: convertError(nextProps.error_info_private) })
    }
    if(!_.isEqual(this.props.data_comm, nextProps.data_comm) && !_.isNil(nextProps.data_comm)) {
      if(_.isArray(nextProps.data_comm.issues)) {
        this.setState({ data_comm: nextProps.data_comm.issues })
      }
    }
    if(!_.isEqual(this.props.data_private, nextProps.data_private) && !_.isNil(nextProps.data_private)) {
      if(_.isArray(nextProps.data_private)) {
        this.setState({ data_private: nextProps.data_private })
      }
    }
    if(!_.isEqual(this.props.data_issue, nextProps.data_issue) && !_.isNil(nextProps.data_issue)) {
      if(nextProps.command === 'delete_issue') this._hapusIssueOffline()
    }
  }

  // tampilkan hal detail issue
  _onIssueClicked(item) {
    this.props.onListClicked(item.id)
    this.props.navigation.navigate('ObrolanDetail')
  }

  // tampilkan dialog konfirmasi hapus issue
  _konfirmasiHapusIssue(item, index) {
    this.setState((state) => {
      return {
        issue_selected: item,
        issue_selected_index: index,
        is_show_delete_confirm: state.is_show_delete_confirm+1,
      }
    })
  }

  // hubungi api menghapus issue
  _hapusIssue() {
    this.setState({
      is_show_loading: true
    }, () =>
      this.props.deleteIssue(
        this.props.token,
        this.state.issue_selected.id
      )
    )
  }

  // update list secara lokal jika berhasil hapus issue
  _hapusIssueOffline() {
    if(this.state.issue_selected_index < 0) return
    let newData = _.clone(this.state.data_comm)
    newData.splice(this.state.issue_selected_index, 1)
    this.setState({ data_comm: newData })
  }

  // update list secara lokal jika berhasil tambah issue
  _tambahIssueOffline(item) {
    let newIssue = {}
    newIssue['id'] = this.props.data_issue.issue_id
    newIssue['community_id'] = this.props.data_user.community_id
    newIssue['community_nama'] = this.props.data_user.community_nama
    newIssue['judul'] = item.issue_judul
    newIssue['featured_img'] = item.issue_foto
    newIssue['status'] = 'open'
    newIssue['author_id'] = this.props.data_user.user_id
    newIssue['published_at'] = getCurrentDate()
    newIssue['jml_komentar'] = 0

    let newData = _.clone(this.state.data_comm)
    newData.push(newIssue)
    this.setState({ data_comm: newData })
  }

  _startInputIssue() {
    this.props.navigation.navigate(
      'InputIssue',
      { refresh: this._tambahIssueOffline.bind(this) }
    )
  }

  // pilih salah satu item chat
  _onChatClicked(item) {
    this.props.onListClicked(item.id)
    this.props.navigation.navigate('ObrolanChat')
  }

  // render item chat
  _renderItemChat = ({ item, index }) => {
    return (
      <TouchableOpacity
        key={item.id}
        style={styles.listContainer}
        onPress={() => this._onChatClicked(item)}>
        <View style={[ styles.listImage, {backgroundColor:'orange'} ]}>
          <ImageLoading
            name={item.trainer_photo ? item.trainer_photo : item.member_photo}
            urlDefault={AppConfig.defaultPhotoURL}
            urlBase={AppConfig.trainerPhotoURL}
            path={item.trainer_photo ? item.trainer_photo : item.member_photo}
          />
        </View>
        <View style={[ styles.listContent, {flexDirection:'row'} ]}>
          <View style={{flex:0.85, flexDirection:'column', backgroundColor:'transparent'}}>
            <Text style={styles.listTitle} numberOfLines={1}>
              {item.trainer_first_name ? item.trainer_first_name : item.member_first_name }
            </Text>
            <Text style={styles.listSubtitle} numberOfLines={1}>
              {item.judul}
            </Text>
            <Text style={styles.listDate} numberOfLines={1}>
              {getReadableDate(item.created_at)}
            </Text>
          </View>
          <View style={{flex:0.15, justifyContent:'center', alignItems:'center', backgroundColor:'transparent'}}>
            { parseInt(item.jml_komentar_unread, 10) > 0 &&
              <View style={{justifyContent:'center', alignItems:'center', width:27, height:27, borderRadius:13.5, backgroundColor:Colors.buttonAction}}>
                <Text style={{alignSelf:'center', color:'white', fontSize:12}}>{item.jml_komentar_unread}</Text>
              </View>
            }
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  // render item issue
  _renderItemIssue = ({ item, index }) => {
    return (
      <Swipeable
        key={item.id}
        rightContent={<SwipedView icon={Images.delete} isLeft={true}/>}
        leftContent={<SwipedView icon={Images.delete} isLeft={true}/>}
        rightActionActivationDistance={100}
        leftActionActivationDistance={100}
        onRightActionRelease={() => this._konfirmasiHapusIssue(item, index)}
        onLeftActionRelease={() => this._konfirmasiHapusIssue(item, index)}>
        <TouchableOpacity
          key={item.id}
          style={styles.listContainer}
          onPress={() => this._onIssueClicked(item)}>
          <View style={styles.listImage}>
            <ImageLoading
              name={item.featured_img}
              urlDefault={AppConfig.defaultPhotoURL}
              urlBase={AppConfig.obrolanPhotoURL}
              path={item.featured_img}
            />
          </View>

          <View style={styles.listContent}>
            <View>
              <Text style={styles.listTitle} numberOfLines={1}>
                {item.community_nama}
              </Text>
              <Text style={styles.listSubtitle} numberOfLines={1}>
                {item.judul}
              </Text>
              <Text style={styles.listDate} numberOfLines={1}>
                {getReadableDate(item.published_at)}
              </Text>
            </View>
            <View style={styles.listAction}>
              <Image source={Images.comment} style={{width:25, height:25, marginRight:5}} />
              <Text>{item.jml_komentar}</Text>
            </View>
          </View>

        </TouchableOpacity>
      </Swipeable>
    );
  }

  // pindah tab ketika memilih judul tab
  _onPageChanged(index) {
    this.setState({ page_index: index })
  }

  render () {
    return (
      <View style={{flex:1, backgroundColor:'#fff'}}>
        <View style={{flexDirection:'row'}}>
          <TouchableOpacity
            onPress={() => this._onPageChanged(0)}
            disabled={Platform.OS === 'ios' ? false : true}
            style={styles.tabContainer}>
            <Text style={styles.tabTitle}>
              Community
            </Text>
            { this.state.page_index === 0 && <View style={{height:5, backgroundColor:'grey'}} /> }
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this._onPageChanged(1)}
            disabled={Platform.OS === 'ios' ? false : true}
            style={styles.tabContainer}>
            <Text style={styles.tabTitle}>
              Private
            </Text>
            { this.state.page_index === 1 && <View style={{height:5, backgroundColor:'grey'}} /> }
          </TouchableOpacity>
        </View>

        <Swiper
          ref={(ref) => this._swiper = ref}
          loop={false}
          index={this.state.page_index}
          showsPagination={false}
          onIndexChanged={(index) => this._onPageChanged(index)}>

          <View style={{flexGrow:1}}>
            <View style={{flex:10}}>
              <View style={{flexGrow:1}}>
                <ListFilter
                 data={this.state.data_comm}
                 errorInfo={this.state.error_info_comm}
                 errorTitle={this.props.error_title_comm}
                 isFetching={this.props.is_fetching_comm}
                 renderRow={this._renderItemIssue}
                 loadingType='pacman'
                 style={{flex:1}}
                />
              </View>
            </View>

           <ButtonIcon
             title='Issue Baru'
             onPress={() => this._startInputIssue()}
             bgColor={Colors.buttonAction}
             styleContainer={stylesCommon.buttonBottomContainer}
             style={stylesCommon.buttonBottom}
             titleColor={Colors.buttonActionTitle}
           />
         </View>

          <View style={{flexGrow:1}}>
            <View style={{flexGrow:1}}>
              <View style={{flexGrow:1}}>
                <ListFilter
                  data={this.state.data_private}
                  errorInfo={this.state.error_info_private}
                  errorTitle={this.props.error_title_private}
                  isFetching={this.props.is_fetching_private}
                  renderRow={this._renderItemChat}
                  loadingType='pacman'
                  style={{flex:1}}
                />
              </View>
            </View>
          </View>

        </Swiper>

        <ConfirmDialog
          isVisible={this.state.is_show_delete_confirm}
          onPressOk={() => this._hapusIssue()}
          title='Hapus Issue'
          message='Apa Anda yakin ingin menghapus issue ini?'
          icon={Images.delete}
          styleButtonOk={{backgroundColor:'red'}}
          styleButtonCancel={{backgroundColor:'gray'}}
        />
        <ModalLoading
          title={this.props.msg_loading}
          isVisible={this.state.is_show_loading}
          isFetching={this.props.is_fetching_issue}
          responseOk={this.props.data_issue}
          responseFail={this.props.error_info_issue}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data_user: state.user.data_user.data,
    id: state.list.id,
    data_comm: state.list.data_array[0],
    data_private: state.list.data_array[1],
    is_fetching_comm: state.list.is_fetching_array[0],
    is_fetching_private: state.list.is_fetching_array[1],
    error_info_comm: state.list.error_info_array[0],
    error_info_private: state.list.error_info_array[1],
    error_title_comm: state.list.error_title_array[0],
    error_title_private: state.list.error_title_array[1],
    msg_loading: state.transaction.msg_loading,
    is_fetching_issue: state.transaction.is_fetching,
    error_info_issue: state.transaction.error_info,
    data_issue: state.transaction.response,
    command: state.transaction.command,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getListComm: (token) => dispatch(ListActions.getObrolanComm(token)),
    getListPrivate: (token) => dispatch(ListActions.getObrolanPrivate(token)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    onListItemClicked: (data_selected) => dispatch(ListActions.onListItemClicked(data_selected)),
    newIssue: (token, judul, body, featured_img) => dispatch(TransactionActions.newIssue(token, judul, body, featured_img)),
    deleteIssue: (token, id) => dispatch(TransactionActions.deleteIssue(token, id)),
    resetList: () => dispatch(ListActions.resetList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObrolanList)
