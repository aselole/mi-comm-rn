import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableWithoutFeedback, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import {
  NotifDialog, ListFilter, ImageFitLoading, ImageLoading,
  ButtonIcon, LoadingOverlay, ModalLoading, ConfirmDialog, Text
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import { getCurrentDate, getReadableDate, getIntervalTimeToday } from '../../Lib/DateUtils'
import Modal from 'react-native-modal'
import Swiper from 'react-native-swiper'
import Swipeable from 'react-native-swipeable'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class ObrolanDetail extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Obrolan Community',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      data: null,
      is_show_modal_comment: false,
      child_comment_selected: null,
      comment_selected_index: -1,
      komentar: '',
      komentar_child: '',
      is_show_delete_confirm: 0,
      is_show_report_confirm: 0,
      data_selected: null,
      msg_loading: '',
      is_show_loading: true,
      is_send_disabled: true,
      is_send_child_disabled: true,
    }
  }

  componentDidMount() {
    this._loadData()
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.detail, nextProps.detail) && !_.isNull(nextProps.detail)) {
      this.setState({ data: nextProps.detail })
    }
    if(!_.isEqual(this.props.data_komen, nextProps.data_komen) && !_.isNull(nextProps.data_komen)) {
      if(this.props.command === 'new_comment') {
        if(nextProps.id_parent_komen === '0') this._appendKomentar(nextProps.data_komen)
        else this._appendKomentarChild(nextProps.data_komen)
      }
    }
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
  }

  _loadData() {
    this.props.getDetail(this.props.token, this.props.id)
  }

  // tampilkan modal detail komentar
  _showModalKomentar(item, index) {
    this.setState({
      is_show_modal_comment: true,
      child_comment_selected: item,
      comment_selected_index: index,
    })
  }

  // sembunyikan modal detail komentar
  _hideModalKomentar() {
    this.setState({
      is_show_modal_comment: false,
      child_comment_selected: null,
      comment_selected_index: -1,
    })
  }

  _konfirmasiLaporKomentar(item) {
    this.setState((state) => {
      return {
        data_selected: item,
        is_show_report_confirm: state.is_show_report_confirm+1,
      }
    })
  }

  // user konfirmasi untuk menghapus komentar
  _konfirmasiHapusKomentar(item) {
    this.setState((state) => {
      return {
        data_selected: item,
        is_show_delete_confirm: state.is_show_delete_confirm+1,
      }
    })
  }

  // user konfirmasi untuk membatalkan hapus komentar
  _batalHapusKomentar() {
    this.setState({ data_selected: null })
  }

  // tampilkan komentar yang baru berhasil diupload
  _appendKomentar(response) {
    let newKomentar = {
      id: response.issue_comment_id,
      comment: this.state.komentar,
      created_at: getCurrentDate(),
      has_child: false,
      user_id: this.props.user_id,
      user_first_name: this.props.data_user.first_name,
      user_photo: this.props.data_user.photo,
    }
    let oldData = this.state.data
    let newData = {}
    newData['issue'] = this.state.data.issue
    newData['issue_comments'] = _.concat(oldData.issue_comments, newKomentar)

    this.setState({
      data: newData,
      komentar: '',
    }, () => {
      this._scroll_komen.scrollToEnd({animated: true})
    })
  }

  // tampilkan balasan komentar yang berhasil diupload
  _appendKomentarChild(response) {
    let parentId = this.state.child_comment_selected.id
    let newKomentarChild = {
      id: response.issue_comment_id,
      parent_id: parentId,
      comment: this.state.komentar_child,
      created_at: getCurrentDate(),
      user_id: this.props.user_id,
      user_first_name: this.props.data_user.first_name,
      user_photo: this.props.data_user.photo,
    }
    let oldKomentar = this.state.data.issue_comments[this.state.comment_selected_index]
    let newKomentar = _.clone(oldKomentar)
    if(oldKomentar.has_child) {
      newKomentar['issue_comment_child'] = _.concat(oldKomentar.issue_comment_child, newKomentarChild)
    } else {
      newKomentar['has_child'] = true
      newKomentar['issue_comment_child'] = [ newKomentarChild ]
    }

    let newListKomentar = _.clone(this.state.data.issue_comments)
    newListKomentar.splice(this.state.comment_selected_index, 1, newKomentar)

    let newData = {}
    newData['issue'] = _.clone(this.state.data.issue)
    newData['issue_comments'] = newListKomentar

    this.setState({
      data: newData,
      komentar_child: '',
      child_comment_selected: newKomentar,
    }, () => {
      this._scroll_modal.scrollToEnd({animated: true})
    })
  }

  // hubungi api untuk upload komentar baru
  _kirimKomentar() {
    this.setState({
      is_show_loading: false
    }, () =>
      this.props.addComment(
        this.props.token,
        'Upload Komentar Baru',
        this.props.id,
        this.state.komentar,
        '0'
      )
    )
  }

  _kirimKomentarChild() {
    this.setState({
      is_show_loading: false
    }, () =>
      this.props.addComment(
        this.props.token,
        'Membalas Komentar',
        this.props.id,
        this.state.komentar_child,
        this.state.child_comment_selected.id
      )
    )
  }

  // hubungi api untuk hapus komentar
  _hapusKomentar() {
    this.setState({
      is_show_loading: true
    }, () =>
      this.props.deleteComment(this.props.token, 'Menghapus Komentar', this.state.data_selected.id)
    )
  }

  // hubungi api untuk melaporkan komentar
  _laporKomentar() {
    this.setState({
      is_show_loading: true
    }, () =>
      this.props.reportComment(this.props.token, this.state.data_selected.id)
    )
  }

  // render item komentar
  _renderItemKomentar = ({ item, index }) => {
    if(_.isNull(item)) return null
    let isChild = item.hasOwnProperty('has_child') ? false : true
    let marginLeft = isChild ? 10 : 0
    let imgMetrics = isChild ? 45 : 55
    let borderRadius = isChild ? 22.5 : 27.5
    let content = (
      <TouchableOpacity
        style={{flex:1, height:70, marginLeft:marginLeft, marginTop:5, flexDirection:'row', backgroundColor:'white'}}
        onPress={() => isChild ? null : this._showModalKomentar(item, index)}>
        <View style={{height:imgMetrics, width:imgMetrics, borderRadius:borderRadius, marginLeft:10, alignSelf:'center', overflow:'hidden'}}>
          <ImageFitLoading
            name={item.user_photo}
            urlDefault={AppConfig.defaultPhotoURL}
            urlBase={AppConfig.trainerPhotoURL}
            path={item.user_photo}
          />
        </View>
        <View style={{flex:1, marginLeft:10, justifyContent:'center'}}>
          <View style={{flex:0.7, justifyContent:'center'}}>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Text style={{fontWeight:'bold'}}>{item.user_first_name}</Text>
              <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>{getIntervalTimeToday(item.created_at)}</Text>
            </View>
            <Text>{item.comment}</Text>
          </View>
          <View style={{flex:0.3, flexDirection:'row', marginRight: 10, alignSelf:'flex-end', justifyContent:'flex-end'}}>
            { item.has_child && !_.isNull(item.issue_comment_child) &&
              <Text>{item.issue_comment_child.length} Balasan</Text>
            }
          </View>
        </View>
      </TouchableOpacity>
    )
    if(this.props.user_id === item.user_id) {   // swipe horizontally untuk menghapus komentar
      return (
        <Swipeable
          key={item.id}
          rightContent={
            <View style={{flex:1, justifyContent:'center', marginTop:5, backgroundColor:'gray'}}>
              <Image
                source={Images.delete}
                style={{marginLeft:40, width:30, height:30}} />
            </View>
          }
          leftContent={
            <View style={{flex:1, justifyContent:'center', alignItems:'flex-end', marginTop:5, backgroundColor:'gray'}}>
              <Image
                source={Images.delete}
                style={{marginRight:40, width:30, height:30}} />
            </View>
          }
          rightActionActivationDistance={100}
          leftActionActivationDistance={100}
          onRightActionRelease={() => this._konfirmasiHapusKomentar(item)}
          onLeftActionRelease={() => this._konfirmasiHapusKomentar(item)}>
          {content}
        </Swipeable>
      )
    } else {    // swipe horizontally untuk laporkan komentar
      return (
        <Swipeable
          key={item.id}
          rightContent={
            <View style={{flex:1, justifyContent:'center', marginTop:5, backgroundColor:'gray'}}>
              <Image
                source={Images.flag}
                style={{marginLeft:40, width:30, height:30}} />
            </View>
          }
          leftContent={
            <View style={{flex:1, justifyContent:'center', alignItems:'flex-end', marginTop:5, backgroundColor:'gray'}}>
              <Image
                source={Images.flag}
                style={{marginRight:40, width:30, height:30}} />
            </View>
          }
          rightActionActivationDistance={100}
          leftActionActivationDistance={100}
          onRightActionRelease={() => this._konfirmasiLaporKomentar(item)}
          onLeftActionRelease={() => this._konfirmasiLaporKomentar(item)}>
          {content}
        </Swipeable>
      )
    }
  }

  // render container dari modal detail komentar
  _renderModalKomentar = () => {
    return (
      <View style={styles.modalSubcontainer}>
        <ScrollView
          ref={(ref) => this._scroll_modal = ref}
          style={styles.modalScrollContainer}
          showsVerticalScrollIndicator={false}>
          { this.state.child_comment_selected &&
            <View
              key={this.state.child_comment_selected.id}
              style={{flex:1, height:70, marginTop:5, flexDirection:'row', backgroundColor:'white'}}>
              <View style={{height:55, width:55, borderRadius:27.5, marginLeft:10, alignSelf:'center', overflow:'hidden'}}>
                <ImageFitLoading
                  name={this.state.child_comment_selected.user_photo}
                  urlDefault={AppConfig.defaultPhotoURL}
                  urlBase={AppConfig.trainerPhotoURL}
                  path={this.state.child_comment_selected.user_photo}
                />
              </View>
              <View style={{flex:1, marginLeft:10, justifyContent:'center'}}>
                <View style={{flex:0.7, justifyContent:'center'}}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text style={{fontWeight:'bold'}}>{this.state.child_comment_selected.user_first_name}</Text>
                    <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>{getIntervalTimeToday(this.state.child_comment_selected.created_at)}</Text>
                  </View>
                  <Text>{this.state.child_comment_selected.comment}</Text>
                </View>
              </View>
            </View>
          }
          <View style={{height:1, backgroundColor:'gray'}}/>
          { this.state.child_comment_selected && this.state.child_comment_selected.issue_comment_child &&
            <View style={{flexGrow:1}}>
              <ListFilter
                data={this.state.child_comment_selected.issue_comment_child}
                isFetching={false}
                renderRow={this._renderItemKomentar}
                isScrollOnRefresh={true}
                styleContainer={{flex:1}}
                style={{flex:1}}
              />
            </View>
          }
        </ScrollView>

        <View style={{flexDirection:'row', backgroundColor:'white'}}>
          <TextInput
            editable={!this.props.is_fetching_komen}
            onChangeText={(komentar_child) => this._onTypingInputChild(komentar_child)}
            value={this.state.komentar_child}
            placeholder='Balas komentar disini...'
            style={{flex:4, paddingLeft:8, fontSize:14}}
            underlineColorAndroid='rgba(0,0,0,0)' />
          <ButtonIcon
            title='Kirim'
            isLoading={this.props.is_fetching_komen}
            isDisabled={this.state.is_send_child_disabled}
            onPress={() => this._kirimKomentarChild()}
            styleContainer={stylesCommon.buttonBottomContainer}
            style={stylesCommon.buttonBottom}
            styleTitle={{fontSize:16}}
            titleColor='#fff'
            bgColor={Colors.buttonAction} />
        </View>
        <ConfirmDialog
          isVisible={this.state.is_show_report_confirm}
          onPressOk={() => this._laporKomentar()}
          title='Laporkan Komentar'
          message='Apakah komentar ini mengundang SARA?'
          titleOk='Ya'
          titleCancel='Tidak'
          icon={Images.flag}
          styleButtonOk={{backgroundColor:'red'}}
          styleButtonCancel={{backgroundColor:'gray'}}
        />
        <ConfirmDialog
          isVisible={this.state.is_show_delete_confirm}
          onPressOk={() => this._hapusKomentar()}
          title='Hapus Komentar'
          message='Apa Anda yakin ingin menghapus komentar Anda?'
          icon={Images.delete}
          styleButtonOk={{backgroundColor:'red'}}
          styleButtonCancel={{backgroundColor:'gray'}}
        />
        <ModalLoading
          title={this.props.msg_loading}
          isVisible={this.state.is_show_loading}
          isFetching={this.props.is_fetching_komen}
          responseOk={this.props.data_komen}
          responseFail={this.props.error_info_komen}
        />
      </View>
    )
  }

  // user mengetik komentar
  _onTypingInput(message) {
    this.setState({
      komentar: message,
      is_send_disabled: _.isEmpty(message) ? true : false,
    })
  }

  // user mengetik balasan komentar
  _onTypingInputChild(message) {
    this.setState({
      komentar_child: message,
      is_send_child_disabled: _.isEmpty(message) ? true : false,
    })
  }

  render () {
    if(this.state.is_fetching) {
      return (
        <View style={styles.container}>
          <DotIndicator color={Colors.theme} count={4} size={10} style={{marginTop: 20}} />
        </View>
      )
    } else if(!this.state.is_fetching && this.state.data) {
      return (
        <View style={{flex:1, flexDirection:'column'}}>
          <View style={{flexGrow:1}}>
            <View style={{flex:10}}>
              <ScrollView
                ref={(ref) => this._scroll_komen = ref}
                style={stylesCommon.detailContainer}
                showsVerticalScrollIndicator={false}>
                <ImageFitLoading
                  name={this.state.data.issue.featured_img}
                  urlDefault={AppConfig.defaultPhotoURL}
                  urlBase={AppConfig.obrolanPhotoURL}
                  path={this.state.data.issue.featured_img}
                  style={styles.detailImage} />
                <View style={styles.detailBlock}>
                  <Text style={styles.detailPrice}>{this.state.data.issue.judul}</Text>
                  <Text style={styles.detailFullPrice}>{this.state.data.issue.community_nama}</Text>
                </View>
                <View style={styles.detailDescBlock}>
                  <Text style={styles.detailDesc}>{this.state.data.issue.body}</Text>
                </View>
                <View style={{paddingVertical:10, flexDirection:'row', justifyContent:'space-between'}}>
                  <Text style={{alignSelf:'flex-start'}}>{getReadableDate(this.state.data.issue.published_at)}</Text>
                  <Text style={{alignSelf:'flex-end'}}>{this.state.data.issue.jml_komentar} Komentar</Text>
                </View>

                <ListFilter
                  data={this.state.data.issue_comments}
                  isFetching={false}
                  renderRow={this._renderItemKomentar}
                  isScrollOnRefresh={true}
                />
              </ScrollView>
            </View>

            <View style={styles.chatInputContainer}>
              <TextInput
                onChangeText={(komentar) => this._onTypingInput(komentar)}
                value={this.state.komentar}
                placeholder='Tulis komentar disini...'
                style={{flex:4, paddingLeft:8, fontSize:14}}
                underlineColorAndroid='rgba(0,0,0,0)' />
              <ButtonIcon
                title='Kirim'
                isLoading={this.props.is_fetching_komen}
                isDisabled={this.state.is_send_disabled}
                onPress={() => this._kirimKomentar()}
                styleContainer={stylesCommon.buttonBottomContainer}
                style={stylesCommon.buttonBottom}
                styleTitle={{fontSize:16}}
                titleColor='#fff'
                bgColor={Colors.buttonAction} />
            </View>
          </View>

          <ConfirmDialog
            isVisible={this.state.is_show_report_confirm}
            onPressOk={() => this._laporKomentar()}
            title='Laporkan Komentar'
            message='Apakah komentar ini mengundang SARA?'
            titleOk='Ya'
            titleCancel='Tidak'
            icon={Images.flag}
            styleButtonOk={{backgroundColor:'red'}}
            styleButtonCancel={{backgroundColor:'gray'}}
          />
          <ConfirmDialog
            isVisible={this.state.is_show_delete_confirm}
            onPressOk={() => this._hapusKomentar()}
            title='Hapus Komentar'
            message='Apa Anda yakin ingin menghapus komentar Anda?'
            icon={Images.delete}
            styleButtonOk={{backgroundColor:'red'}}
            styleButtonCancel={{backgroundColor:'gray'}}
          />
          <ModalLoading
            title={this.props.msg_loading}
            isVisible={this.state.is_show_loading}
            isFetching={this.props.is_fetching_komen}
            responseOk={this.props.data_komen}
            responseFail={this.props.error_info_komen}
          />

          <Modal
            isVisible={this.state.is_show_modal_comment}
            backdropColor='gray'
            backdropOpacity={0.7}
            onBackdropPress={() => this._hideModalKomentar()}
            onBackButtonPress={() => this._hideModalKomentar()}
            hideModalContentWhileAnimating={false}
            style={styles.modalContainer}>
            {this._renderModalKomentar()}
          </Modal>
        </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    user_id: state.user.data_user.data.user_id,
    data_user: state.user.data_user.data,
    detail: state.list.detail,
    data: state.list.data,
    data_selected: state.list.data_selected,
    id: state.list.id,
    is_fetching: state.list.is_fetching,
    error_info: state.list.error_info,
    data_komen: state.transaction.response,
    is_fetching_komen: state.transaction.is_fetching,
    error_info_komen: state.transaction.error_info,
    msg_loading: state.transaction.msg_loading,
    id_parent_komen: state.transaction.parent_id,
    command: state.transaction.command,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getObrolanCommDetail(token, id)),
    addComment: (token, msg_loading, issue_id, comment, parent_id) => dispatch(TransactionActions.newComment(token, msg_loading, issue_id, comment, parent_id)),
    deleteComment: (token, msg_loading, id) => dispatch(TransactionActions.deleteComment(token, msg_loading, id)),
    reportComment: (token, issue_comment_id) => dispatch(TransactionActions.reportComment(token, issue_comment_id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObrolanDetail)
