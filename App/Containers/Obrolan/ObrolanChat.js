import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableWithoutFeedback, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import ChatActions from '../../Redux/ChatRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import {
  NotifDialog, ListFilter, ImageFitLoading, ImageLoading,
  ButtonIcon, LoadingOverlay, ModalLoading, ConfirmDialog
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import { getCurrentDate, isSameDay, getReadableDate, getIntervalDateToday, getIntervalTimeToday } from '../../Lib/DateUtils'
import Modal from 'react-native-modal'
import Swiper from 'react-native-swiper'
import Swipeable from 'react-native-swipeable'
import moment from 'moment'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class ObrolanChat extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Obrolan Private',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      list: null,
      list_last_index: 0,
      parent: null,
      message: null,
      error_info: null,
      is_send_disabled: true,
    }
  }

  componentDidMount() {
    this.props.resetNotifChat() // menghindari bug, klau ada notifikasi baru, chat terakhir diduplikat
    this.props.getDetail(this.props.token, this.props.id)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this.setState({
        parent: nextProps.data.private_issue,
        list: this._groupBasedOnDate(nextProps.data.private_issue_comments),
        list_last_index: this._getLastChatIndex(nextProps.data.private_issue_comments),
      })
    }
    if(!_.isEqual(this.props.response, nextProps.response) && !_.isNull(nextProps.response)) {
      this._appendChatFromTextInput()
    }
    if(!_.isEqual(this.props.chat, nextProps.chat) && !_.isEmpty(nextProps.chat)) {
      this._appendChatFromNotification(nextProps.chat)
    }
    if(!_.isEqual(this.props.chat_read, nextProps.chat_read) && !_.isEmpty(nextProps.chat_read)) {
      this._updateChatReadStatus(nextProps.chat_read)
    }
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ error_info: convertError(nextProps.error_info) })
    }
  }

  // cek jenis user apa yg login
  _isUser() {
    if(this.state.parent.user_member_id === this.props.data_user.user_id) {
      return true
    } else {
      return false
    }
  }

  _getLastChatIndex(data) {
    if(_.isNull(data) || _.isNil(data)) return 0
    return _.maxBy(data, 'id')['id']
  }

  // kelompokkan data berdasar tanggal
  _groupBasedOnDate(data) {
    let dateParam = (item) => getIntervalDateToday(moment(item.created_at).startOf('day').format())
    let groupByDate = (group, index) => {
        return {
            title: index,
            data: group
        }
    };
    return _.chain(data)
            .groupBy(dateParam)
            .map(groupByDate)
            .sortBy('day')
            .value()
  }

  // data user yang diajak bicara
  _getUserLawanBicara() {
    if(this.props.data_user.user_id === this.state.parent.user_member_id) {
      return {
        author_id: this.state.parent.user_trainer_id,
        author_name: this.state.parent.trainer_first_name,
        author_photo: this.state.parent.trainer_photo,
      }
    } else {
      return {
        author_id: this.state.parent.user_member_id,
        author_name: this.state.parent.member_first_name,
        author_photo: this.state.parent.member_photo,
      }
    }
  }

  // data user yang sedang login
  _getUserYangBicara() {
    return {
      author_id: this.props.data_user.user_id,
      author_name: this.props.data_user.first_name,
      author_photo: this.props.data_user.photo
    }
  }

  /*
  * menampilkan pesan baru yg muncul di notifikasi,
  * jika halaman yg lagi dibuka ialah obrolan private yg dituju pesan baru tsb
  */
  _appendChatFromNotification(chat) {
    let correspondIndex = _.findIndex(chat, (item) => item.id === this.props.id)
    if(correspondIndex >= 0) {   // pesan di notifikasi ditujukan ke obrolan private yg sedang dibuka
      chat[correspondIndex]['data'].map((item) => {
        this._appendChatBaru(item.body, item.created_at, this._getUserLawanBicara())
      })
    }
  }

  // menampilkan pesan baru yang baru diketik dan berhasil dikirim
  _appendChatFromTextInput() {
    this._appendChatBaru(this.state.message, getCurrentDate(), this._getUserYangBicara())
  }

  // tampilkan pesan yang baru tanpa perlu fetching dari server
  _appendChatBaru(message, created_at, author) {
    let newData = []
    if(!_.isNull(this.state.list) && !_.isEmpty(this.state.list)) { // jika ada history chat
      newData = _.cloneDeep(this.state.list)
      let index = _.findIndex(this.state.list, (item) => isSameDay(item.data[0]['created_at'], created_at)) // cari history chat yang tanggalnya sama dgn hari ini
      if(index >= 0) {  // jika ada history chat dgn tanggal hari ini
        let newMessage = this._generateChat(
                                  _.cloneDeep(this.state.list[index]['data'][0]), this.state.list_last_index,
                                  message, created_at, author
                                )
        newData[index].data.push(newMessage)
      } else {  // jika tidak ada history chat dgn tanggal hari ini
        let newGroupChat = this._generateChatGroup(message, created_at, author)
        newData.push(newGroupChat)
      }
    } else {    // jika tidak ada history chat sama sekali
      let newGroupChat = this._generateChatGroup(message, created_at, author)
      newData.push(newGroupChat)
    }

    this.setState({
      message: null,
      list: newData,
      list_last_index: this.state.list_last_index + 1
    })
  }

  // generate struktur data dari grup pesan berdasar tanggal
  _generateChatGroup(message, created_at, author) {
    let groupChat = {}
    groupChat['title'] = getIntervalDateToday(moment().startOf('day').format())
    groupChat['data'] = []
    let newMessage = this._generateChat(null, this.state.list_last_index, message, created_at, author)
    groupChat['data'].push(newMessage)
    return groupChat
  }

  // generate struktur data dari sebuah pesan
  _generateChat(input, last_index, message, created_at, author) {
    let newMessage = _.isNil(input) ? {} : input
    newMessage['id'] = parseInt(last_index) + 1
    newMessage['comment'] = message
    newMessage['created_at'] = created_at
    newMessage['read'] = '0'
    newMessage['user_id'] = author.author_id
    newMessage['user_first_name'] = author.author_name
    newMessage['user_photo'] = author.author_photo
    return newMessage
  }

  // kirim pesan baru ke lawan bicara
  _kirimPesanBaru() {
    this.props.pushChatMessage(
      this.props.token,
      this.props.id,
      this.state.message
    )
  }

  // update status pesan yg kita kirim telah dibaca lawan bicara
  _updateChatReadStatus(chatRoomReadId) {
    let roomReadId = _.findIndex(chatRoomReadId, (item) => item == this.props.id)
    if(roomReadId >= 0) {
      let chats = _.cloneDeep(this.state.list)
      if(_.isNil(chats) || _.isEmpty(chats)) return
      chats.slice(0).reverse().some((parent, indexParent) => {
        let isRead = false
        parent.data.slice(0).reverse().some((child, indexChild) => {
          if(child.read === '0') {
            chats[(chats.length-1)-indexParent]['data'][(parent.data.length-1)-indexChild]['read'] = '1'
          } else if(child.read === '1') {
            isRead = true
            return true
          }
        })
        if(isRead) return true
      })
      this.setState({ list: chats })
    }
  }

  // render header tanggal
  _renderChatHeader = ({section: {title}}) => {
    return (
      <View style={styles.chatHeaderContainer}>
        <Text>{title}</Text>
      </View>
    )
  }

  // render isi pesan tiap tanggal
  _renderChat = ({ item }) => {
    if(item.user_id === this.props.data_user.user_id) {   // chat dari user yg sdng login
      let checkImage = item.read === '1' ? Images.check_read : Images.check
      return (
        <View style={styles.chatContainer}>
          <Text style={styles.chatBodyMessage}>{item.comment}</Text>
          <Text style={styles.chatBodyDate}>{getReadableDate(item.created_at, null, 'HH:mm')}</Text>
          <Image
            source={checkImage}
            style={styles.chatBodyCheck}
          />
        </View>
      )
    } else {    // chat dari yg diajak obrol
      return (
        <View style={styles.chatReplyContainer}>
          <Text style={styles.chatReplyBodyMessage}>{item.comment}</Text>
          <Text style={styles.chatReplyBodyDate}>{getReadableDate(item.created_at, null, 'HH:mm')}</Text>
        </View>
      )
    }
  }

  // user mengetik pesan
  _onTypingInput(message) {
    this.setState({
      message,
      is_send_disabled: _.isEmpty(message) ? true : false,
    })
  }

  render () {
    return (
      <View style={{flexGrow:1, flexDirection:'column'}}>
        <View style={{flexGrow:5}}>
          { !_.isNil(this.state.parent) &&
            <View style={styles.chatRoomHeader}>
              <View style={styles.chatRoomHeaderImageContainer}>
                <ImageLoading
                  name={this._isUser() ? this.state.parent.trainer_photo : this.state.parent.member_photo }
                  urlDefault={AppConfig.defaultPhotoURL}
                  urlBase={AppConfig.trainerPhotoURL}
                  path={this._isUser() ? this.state.parent.trainer_photo : this.state.parent.member_photo }
                  borderRadius={20}
                />
              </View>
              <View style={styles.chatRoomHeaderBody}>
                <Text style={{fontWeight: 'bold'}}>
                  { this._isUser() ?
                    this.state.parent.trainer_first_name :
                    this.state.parent.member_first_name
                  }
                </Text>
                <Text>
                  {this.state.parent.judul}
                </Text>
              </View>
            </View>
          }
          <View style={{flexGrow:20}}>
            <ListFilter
              data={this.state.list}
              errorInfo={this.state.error_info}
              errorTitle={this.props.error_title}
              isFetching={this.props.is_fetching}
              renderRow={this._renderChat}
              renderHeader={this._renderChatHeader}
              isScrollOnRefresh={true}
              isMultiSection={true}
              loadingType='pacman'
              style={{flex:1}}
              styleContent={{}}
            />
          </View>
        </View>
        <View style={styles.chatInputContainer}>
          <TextInput
            onChangeText={(message) => this._onTypingInput(message)}
            value={this.state.message}
            placeholder='Balas disini...'
            style={{flex:4, paddingLeft:8, fontSize:14}}
            underlineColorAndroid='rgba(0,0,0,0)' />
          <ButtonIcon
            title='Kirim'
            isLoading={this.props.is_fetching_chat}
            isDisabled={this.state.is_send_disabled}
            onPress={() => this._kirimPesanBaru()}
            styleContainer={stylesCommon.buttonBottomContainer}
            style={stylesCommon.buttonBottom}
            styleTitle={{fontSize:16}}
            titleColor='#fff'
            bgColor={Colors.buttonAction} />
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data_user: state.user.data_user.data,
    data: state.list.detail,
    id: state.list.id,
    is_fetching: state.list.is_fetching,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
    response: state.transaction.response,
    error: state.transaction.error_info,
    is_fetching_chat: state.transaction.is_fetching,
    chat: state.chat.messages,
    chat_read: state.chat.chat_room_read_id,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getObrolanPrivateDetail(token, id)),
    pushChatMessage: (token, private_issue_id, comment) => dispatch(TransactionActions.newChatMessage(token, private_issue_id, comment)),
    resetNotifChat: () => () => dispatch(ChatActions.reset()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObrolanChat)
