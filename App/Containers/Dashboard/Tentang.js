import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { TextInputIcon, ButtonBox, Text } from '../../Components/'
import FlexImage from 'react-native-flex-image'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import css from '../../Themes/Style'

class Tentang extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Tentang',
      headerStyle: css.noShadow,
    }
  }

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <View style={[ styles.container, {paddingHorizontal:10, paddingBottom:10, paddingTop:20} ]}>
        <FlexImage source={Images.logo} style={styles.logo} />
        <Text style={{marginTop: 30, lineHeight: 25}}>Mi-Comm adalah komunitas yang terbentuk dengan visi memberikan kontribusi yang nyata dalam membentuk perubahan sosial ke arah yang lebih baik. Dengan memberikan informasi, motivasi, solusi, fasilitas, dan juga manfaat lainnya. Kami yakin bahwa mi-Comm akan mengembangkan kreatifitas, menjadikan benefit serta profit dalam kehidupan saat ini dan masa yang akan datang.</Text>
        <Text style={{marginTop: 20, paddingBottom: 5}}>Alamat:</Text>
        <Text>Jl. Ngagel Madya No.17, Baratajaya, Surabaya</Text>
        <Text style={{marginTop: 20, paddingBottom: 5}}>Telepon:</Text>
        <Text>+62 821 1256 7388</Text>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // signin: (username, password, token_firebase) => dispatch(UserAction.signin(username, password, token_firebase)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tentang)
