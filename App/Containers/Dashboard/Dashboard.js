import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { isAndroid } from '../../Lib/PlatformUtils'
import {
  TextInputIcon, ButtonBox, ButtonIcon, Text,
  ToolbarButton, BoxDashboard
} from '../../Components/'
import Modal from 'react-native-modal'
import FlexImage from 'react-native-flex-image'
import { registerAppListener, registerKilledListener, recoverBackFromDeadNotification } from '../../Notification/Listeners'
import { createNotificationChannel } from '../../Notification/Channel'
import { isActiveScreen } from '../../Lib/AppCustomUtils'
import DebugConfig from '../../Config/DebugConfig'
var _ = require('lodash');

// Styles
import styles from './Style'
import css from '../../Themes/Style'
import stylesCommon from '../../Themes/StyleCommon'

registerKilledListener()

class Dashboard extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      headerLeft: <View></View>,
      title: 'Mi-Comm',
      headerStyle: css.noShadow,
      headerTitleStyle: css.toolbarTitle,
      headerRight:
        <View style={stylesCommon.toolbarRightContainer}>
          <ToolbarButton
            image={Images.card}
            onPress={() => params.onShowCard()}
            styleContainer={{flex:0.6, marginRight:16, backgroundColor:'transparent'}}
          />
          <ToolbarButton
            image={Images.profile}
            onPress={() => params.onShowProfil()}
            styleContainer={{flex:0.44, backgroundColor:'transparent'}}
          />
        </View>
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_show_kartu: false,
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onShowCard: this._onShowKartu,
      onShowProfil: this._onProfilClicked,
    })
    if(isAndroid()) {
      createNotificationChannel()
      registerAppListener(this._onNotificationClicked.bind(this))
      recoverBackFromDeadNotification(this._onNotificationClicked.bind(this))
    }
  }

  // jika notifikasi dari server diklik
  _onNotificationClicked(notif) {
    if (DebugConfig.useReactotron) {
      console.tron.display({
        name: 'Dashboard _onNotificationClicked',
        value: { notif: notif }
      })
    }
    this.props.onListClicked(notif.id)
    if(_.isEqual(notif.jenis, 'private_issue')) {
      if(!isActiveScreen(this.props, 'ObrolanChat')) {
        this.props.navigation.navigate('ObrolanChat')
      }
    } else {
      this.props.navigation.navigate('EventDetail')
    }
  }

  _onMenuClicked(title) {
    this.props.navigation.navigate(title)
  }

  _onProfilClicked = () => {
    this.props.navigation.navigate('ProfilDetail')
  }

  _onShowKartu = () => {
    this.setState({ is_show_kartu: true })
  }

  _onCloseKartu() {
    this.setState({ is_show_kartu: false })
  }

  render () {
    return (
      <View style={styles.container}>

        <ScrollView style={{flex:1, backgroundColor:'#fff'}}>
          <View style={css.templateLayout}>
            <BoxDashboard
              title='Berita'
              image={Images.menu_berita}
              onPress={() => this._onMenuClicked('BeritaList')} />
            <BoxDashboard
              title='Event & Promo'
              image={Images.menu_promo}
              onPress={() => this._onMenuClicked('EventList')} />
            <BoxDashboard
              title='Obrolan'
              image={Images.menu_issues}
              onPress={() => this._onMenuClicked('ObrolanList')} />
            <BoxDashboard
              title='Trainer'
              image={Images.menu_trainer}
              onPress={() => this._onMenuClicked('TrainerList')} />
            <BoxDashboard
              title='Partnership Deals'
              image={Images.menu_merchant}
              onPress={() => this._onMenuClicked('DealList')} />
            <BoxDashboard
              title='Partner'
              image={Images.menu_partner}
              onPress={() => this._onMenuClicked('PartnerList')} />
            <BoxDashboard
              title='Peta'
              image={Images.menu_map}
              onPress={() => this._onMenuClicked('Peta')} />
            <BoxDashboard
              title='Tentang'
              image={Images.menu_tentang}
              onPress={() => this._onMenuClicked('Tentang')} />
          </View>
        </ScrollView>
        { this.props.user &&
          <Modal
            isVisible={this.state.is_show_kartu}
            backdropColor='gray'
            backdropOpacity={0.5}
            animationInTiming={1000}
            onBackdropPress={() => this._onCloseKartu()}
            onBackButtonPress={() => this._onCloseKartu()}
            onSwipe={() => this._onCloseKartu()}
            swipeDirection="down"
            hideModalContentWhileAnimating={true}
            style={styles.modalContainer}>
            <View style={styles.kartuContainer}>
              <FlexImage source={Images.kartu} />
              <Text style={styles.kartuName}>
                {this.props.user.first_name}
              </Text>
              <Text style={styles.kartuBarcode}>
                {this.props.user.card_barcode}
              </Text>
              { this.props.user.card_barcode &&
                <FlexImage
                  source={{uri: 'http://barcodes4.me/barcode/c128c/'+this.props.user.card_barcode+'.png'}}
                />
              }
            </View>
          </Modal>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user.data_user.data,
    active_screen: state.screen.active,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
