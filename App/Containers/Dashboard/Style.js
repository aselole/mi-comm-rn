import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor:'#fff'
  },
  logo: {
    width: deviceWidth / 1.5,
    alignSelf: 'center',
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  textInput: {

  },
  textRegisterTitle: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    marginTop: 15,
  },
  textRegister: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  textForgotPass: {
    textAlign: 'right', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  dropdown: {
    height:50,
    width:null,
    borderColor: 'lightgray',
    borderRadius: 2,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  kartuBarcode: {
    width: deviceWidth - 50,
    height: 200,
  },
  kartuBg: {
    width: deviceWidth
  },
  modalContainer: {
    margin: 0,
  },
  kartuContainer: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 15,
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  kartuName: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 140,
    left: 40,
  },
  kartuBarcode: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 160,
    left: 40,
  },
})
