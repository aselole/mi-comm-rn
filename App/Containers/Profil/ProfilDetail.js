import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import UploadActions from '../../Redux/UploadRedux'
import UserActions from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ButtonIcon, ModalDropdown, ButtonBox,
  ImageLoading, ImageFitLoading, ListFilter, ImagePicker,
  ModalLoading, ToolbarButton, Text
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import FlexImage from 'react-native-flex-image'
import Modal from 'react-native-modal'
import { NavigationActions } from 'react-navigation'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class ProfilDetail extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Profil',
      headerStyle: css.noShadow,
      headerRight:
        <ToolbarButton
          image={Images.edit_pass}
          onPress={() => params.onEditPass()}
          styleContainer={{flex:0.7, marginRight:16}}
        />
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: true,
      ok_data_member: false,
      data_member: null,
      is_show_comm: false,
      is_show_bio: false,
      foto: null,
      is_fetching_foto: false,
      is_foto_edited: false,
      is_foto_edit_shown: false,
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onEditPass: this._onStartEditPass.bind(this),
    })
    this.props.getCommMember(this.props.token)
    this._checkEditedFoto()
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data_member, nextProps.data_member) && !_.isNull(nextProps.data_member)) {
      this.setState({
        ok_data_member: true,
        data_member: nextProps.data_member.community,
      })
    }
    if(!_.isEqual(this.props.is_fetching_foto, nextProps.is_fetching_foto)) {
      this.setState({ is_fetching_foto: nextProps.is_fetching_foto })
    }
    if(!_.isEqual(this.props.foto_edit, nextProps.foto_edit) && !_.isNull(nextProps.foto_edit)) {
      this.setState({
        foto: nextProps.foto_edit,
        is_foto_edited: true,
      })
    }
    if(!_.isEqual(this.props.data_user, nextProps.data_user) && _.isEmpty(nextProps.data_user)) { // user telah selesai logout
      // console.tron.log('ProfilDetail componentWillReceiveProps data_user null')
      // this.props.navigation.navigate('Splash')
      this.props.navigation.dispatch({
        type: NavigationActions.NAVIGATE,
        routeName: 'Splash',
        action: {
          type: NavigationActions.RESET,
          index: 0,
          actions: [{type: NavigationActions.NAVIGATE, routeName: 'Splash'}]
        }
      })
    }
  }

  _checkEditedFoto() {
    if(!_.isNull(this.props.foto_edit)) {
      this.setState({
        is_foto_edited: true,
        is_foto_edit_shown: true,
        foto: this.props.foto_edit
      })
    }
  }

  // tampilkan modal info komunitas
  _onOpenComm() {
    if(this.state.ok_data_member)
      this.setState({ is_show_comm: true })
  }

  // sembunyikan modal info komunitas
  _onCloseComm() {
    this.setState({ is_show_comm: false })
  }

  _renderCommList = ({ item }) => {
    return (
      <View style={styles.commItemContainer}>
        <TouchableOpacity
          key={item.user_id}
          style={styles.commItemSubContainer}>
          <View style={{flex:0.4}}>
            <ImageLoading
              name={item.user_photo}
              urlDefault={AppConfig.defaultPhotoURL}
              urlBase={AppConfig.trainerPhotoURL}
              path={item.user_photo}
            />
          </View>
          <View style={{flex:0.6, justifyContent:'center'}}>
            <Text style={[stylesTheme.mediumBold, {marginLeft: 10, fontSize:12, paddingBottom:0}]}>
              {item.user_first_name}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  _onStartEditBio() {
    this.props.navigation.navigate('EditBio')
  }

  _onStartEditPass() {
    this.props.navigation.navigate('EditPassword')
  }

  _onFotoReceived(item) {
    this.props.uploadFotoUser(this.props.token, item)
    this.props.updateFoto(item[0])
  }

  _onEditSuccess() {
    this.setState({ is_foto_edit_shown: true })
  }

  _onLogout() {
    this.props.resetUser()
  }

  _getActiveImage() {
    let urlImage = null
    if((!this.state.is_foto_edit_shown || !this.state.is_foto_edited) && this.props.data)
      urlImage = AppConfig.trainerPhotoURL + this.props.data.photo
    else if(this.state.is_foto_edit_shown && this.state.is_foto_edited )
      urlImage = this.state.foto
    return urlImage
  }

  _renderCommunityDetail = () => {
    if(this.state.data_member) {
      return (
        <Modal
          isVisible={this.state.is_show_comm}
          backdropColor='gray'
          backdropOpacity={0.5}
          animationInTiming={1000}
          onBackdropPress={() => this._onCloseComm()}
          onBackButtonPress={() => this._onCloseComm()}
          onSwipe={() => this._onCloseComm()}
          swipeDirection="down"
          hideModalContentWhileAnimating={true}
          style={styles.modalContainer}>
          <View style={styles.commContainer}>
            <FlexImage
              style={{height:150, margin: 15, marginBottom:10, alignSelf:'center'}}
              source={{uri: AppConfig.communityPhotoURL+this.state.data_member.logo}}
              loadingComponent={
                <DotIndicator color={Colors.theme} count={4} size={10} />
              } />
            <Text style={{marginHorizontal: 15, marginBottom:10, alignSelf:'center'}}>{this.state.data_member.ket}</Text>
            <Text style={{marginHorizontal: 15}}>Member</Text>
            <ListFilter
              data={this.state.data_member.members}
              isFetching={this.props.is_fetching}
              renderRow={this._renderCommList.bind(this)}
              isHorizontal={true}
              keys='user_id'
              styleContainer={{}}
            />
          </View>
        </Modal>
      )
    } else return null
  }

  render () {
    return (
      <ScrollView
        style={stylesCommon.detailContainer}
        showsVerticalScrollIndicator={false}>
        <View style={styles.imageContainer}>
          <Image
            source={{uri: this._getActiveImage()}}
            style={styles.detailImage} />
        </View>
        <View style={{alignSelf:'flex-end', margin:5}}>
          <ImagePicker
            onChange={(item) => this._onFotoReceived(item)}
            data={this.state.foto}
            titleBottomSheet='Pilih Foto'
            isMultiplePick={false}
            isCropping={true}
            isShowGallery={false}
            isShowCancelButton={false}
            styleContainer={{}}
            styleButtonContainer={{flex:1, flexDirection:'row'}}
            styleButtonOk={{}}
            colorButtonOk='red'
            titleButtonOk='EDIT FOTO'
            isCustomComponent={true}
            customComponent={(props) => (
              <TouchableOpacity onPress={() => props.onStartPick()}>
                <Image source={Images.edit_photo} style={{marginHorizontal:10, marginBottom:8, width:25, height:25}} />
              </TouchableOpacity>
            )}
          />
        </View>
        { this.props.data &&
          <View style={styles.body}>

            <View style={styles.card}>
              <View style={styles.row}>
                <Text style={[styles.label, styles.header]}>Biodata</Text>
                <TouchableOpacity onPress={() => this._onStartEditBio()}>
                  <Image source={Images.edit_profile} style={{marginRight:10, width:25, height:25}} />
                </TouchableOpacity>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Nama</Text>
                  <Text style={styles.subLabel}>{this.props.data.first_name}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>E-mail</Text>
                  <Text style={styles.subLabel}>{this.props.data.email}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Tipe</Text>
                  <Text style={styles.subLabel}>{this.props.data.level_nama}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Peserta</Text>
                  <Text style={styles.subLabel}>{this.props.data.product_nama}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Jenis Kelamin</Text>
                  <Text style={styles.subLabel}>{this.props.data.gender === 'L' ? 'Laki-Laki':'Perempuan'}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Telepon</Text>
                  <Text style={styles.subLabel}>{this.props.data.phone}</Text>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.labelView}>
                  <Text style={styles.description}>Alamat</Text>
                  <Text style={styles.subLabel}>{this.props.data.address}</Text>
                </View>
              </View>
            </View>

            { this.state.data_member &&
              <View style={styles.card}>
                <View style={styles.row}>
                  <Text style={[styles.label, styles.header]}>Community</Text>
                  <Text style={[ styles.subLabel, {marginRight:30} ]}>{this.state.data_member.nama}</Text>
                </View>
                <TouchableOpacity onPress={() => this._onOpenComm()}>
                  <FlexImage
                    style={{height:100, marginBottom:10, alignSelf:'center'}}
                    source={{uri: AppConfig.communityPhotoURL+this.state.data_member.logo}}
                    loadingComponent={
                      <DotIndicator color={Colors.theme} count={4} size={10} />
                    } />
                </TouchableOpacity>
              </View>
            }
            { !this.state.data_member &&
              <View style={styles.card}>
                <DotIndicator color={Colors.theme} count={4} size={10} />
              </View>
            }

          </View>
        }
        <ButtonIcon
          title='Keluar Sesi'
          onPress={() => this._onLogout()}
          bgColor={Colors.red}
          styleContainer={stylesCommon.buttonBottomContainer}
          style={stylesCommon.buttonBottom}
          bgColor={Colors.buttonAction}
          loadingBgColor={Colors.buttonDisabled}
          titleColor={Colors.buttonActionTitle}
          loadingTitleColor={Colors.buttonDisabledTitle}
        />
        {this._renderCommunityDetail()}
        <ModalLoading
          title='Mengganti Foto Profil Anda'
          isFetching={this.props.is_fetching_foto}
          responseOk={this.props.data_foto}
          responseFail={this.props.error_info_foto}
          onConfirmResponseOk={() => this._onEditSuccess()}
        />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data: state.user.data_user.data,
    data_user: state.user.data_user,
    data_member: state.list.data,
    is_fetching: state.list.is_fetching,
    response_bio: state.transaction.response,
    is_fetching_foto: state.upload.is_fetching,
    data_foto: state.upload.response,
    error_info_foto: state.upload.error_info,
    foto_edit: state.user.foto_edit,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCommMember: (token) => dispatch(ListActions.getCommunityMember(token)),
    editBio: (token, nama, gender, phone, address) => dispatch(TransactionActions.editBio(token, nama, gender, phone, address)),
    uploadFotoUser: (token, data) => dispatch(UploadActions.uploadFotoUser(token, data)),
    updateFoto: (foto_edit) => dispatch(UserActions.updateFoto(foto_edit)),
    resetUser: () => dispatch(UserActions.resetUser()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilDetail)
