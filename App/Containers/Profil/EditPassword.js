import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import UserActions from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ButtonIcon, ModalDropdown, ButtonBox,
  ImageLoading, ImageFitLoading, ListFilter,
  ModalLoading, ButtonRoundBlue, Text
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import FlexImage from 'react-native-flex-image'
import Modal from 'react-native-modal'
import Immutable from 'seamless-immutable'
var _ = require('lodash');

import styles from './Style'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class EditPassword extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Edit Password',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: false,
      password: '', confirm: '',
      error_password: '', error_confirm: '',
      is_error_password: false, is_error_confirm: false
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
  }

  _onEditPassword() {
    if(this._isValid()) {
      this.setState({ msg_error: null, msg_ok: null })
      this.props.uploadPass(
        this.props.token,
        this.state.password,
        this.state.confirm
      )
    }
  }

  _isValid() {
    this.setState({
      error_password: '',
      is_error_password: false,
      error_confirm: '',
      is_error_confirm: false
    })
    let isPermitted = true
    if(_.isEmpty(this.state.password)) {
      this.setState({
        error_password: 'Field harus diisi',
        is_error_password: true
      })
      isPermitted = false
    }
    if(!_.isEqual(this.state.password, this.state.confirm)) {
      this.setState({
        error_confirm: 'Password tidak cocok',
        is_error_confirm: true
      })
      isPermitted = false
    }
    else if(_.isEmpty(this.state.confirm)) {
      this.setState({
        error_confirm: 'Field harus diisi',
        is_error_confirm: true
      })
      isPermitted = false
    }
    return isPermitted
  }

  _onEditSuccess() {
    this.props.navigation.goBack()
  }

  render () {
    return (
      <View style={{flexGrow:1, backgroundColor:'#fff', paddingTop:20}}>

        <View style={{flexGrow:1}}>
          <View style={{flex:10}}>
            <ScrollView
              style={stylesCommon.detailContainer}
              showsVerticalScrollIndicator={false}>
              <View style={{flexGrow:1, flex:1, flexDirection:'column'}}>
                <TextInput
                  onChangeText={(password) => this.setState({password})}
                  value={this.state.password}
                  style={[ styles.input, {borderColor: this.state.is_error_password ? 'red' : '#ddd'} ]}
                  placeholder={'Password Baru'}
                  secureTextEntry={true}
                  underlineColorAndroid='rgba(0,0,0,0)' />
                <Text style={styles.inputError}>{this.state.error_password}</Text>
                <TextInput
                  onChangeText={(confirm) => this.setState({confirm})}
                  value={this.state.confirm}
                  style={[ styles.input, {borderColor: this.state.is_error_confirm ? 'red' : '#ddd'} ]}
                  placeholder={'Ulangi Password'}
                  secureTextEntry={true}
                  underlineColorAndroid='rgba(0,0,0,0)' />
                  <Text style={styles.inputError}>{this.state.error_confirm}</Text>
              </View>
            </ScrollView>
          </View>

          <View style={stylesCommon.buttonDoubleBottomContainer}>
            <ButtonRoundBlue
              onPress={() => this.props.navigation.goBack()}
              style={css.buttonCancel}
              text='Cancel'
              styleText={{color:Colors.buttonDisabledTitle}}/>
            <ButtonRoundBlue
              onPress={() => this._onEditPassword()}
              style={css.buttonOk}
              text='OK'/>
          </View>
        </View>

        <ModalLoading
          title='Merubah Password Anda'
          isFetching={this.props.is_fetching}
          responseOk={this.props.data_bio}
          responseFail={this.props.error_info}
          onConfirmResponseOk={() => this._onEditSuccess()}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data_user: state.user.data_user,
    token: state.user.data_user.token,
    data: state.user.data_user.data,
    data_bio: state.transaction.response,
    is_fetching: state.transaction.is_fetching,
    error_info: state.transaction.error_info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadPass: (token, password, confirm_password) => dispatch(TransactionActions.editPass(token, password, confirm_password)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPassword)
