import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import UserActions from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ButtonIcon, ModalDropdown, ButtonBox,
  ImageLoading, ImageFitLoading, ListFilter,
  ModalLoading, ButtonRoundBlue, RadioButton
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import FlexImage from 'react-native-flex-image'
import Modal from 'react-native-modal'
import Immutable from 'seamless-immutable'
var _ = require('lodash');

import styles from './Style'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class EditBio extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Edit Biodata',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      is_fetching: false,
      nama: '', kelamin: 0, telepon: '', alamat: '',
      is_error_nama: false, is_error_kelamin: false,
      is_error_telepon: false, is_error_alamat: false,
    }
  }

  componentDidMount() {
    let kelamin = 0
    if(this.props.data.gender === 'L') kelamin = 0
    else if(this.props.data.gender === 'P') kelamin = 1
    this.setState({
      is_show_bio: true,
      nama: this.props.data.first_name,
      alamat: this.props.data.address,
      telepon: this.props.data.phone,
      kelamin: kelamin,
    })
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.is_fetching, nextProps.is_fetching)) {
      this.setState({ is_fetching: nextProps.is_fetching })
    }
  }

  _onEditBio() {
    if(this._isValid()) {
      let kelamin = this.state.kelamin === 0 ? 'L' : 'P'
      this.setState({ msg_error: null, msg_ok: null })
      this.props.uploadBio(
        this.props.token,
        this.state.nama,
        kelamin,
        this.state.telepon,
        this.state.alamat
      )
    }
  }

  _isValid() {
    this.setState({
      is_error_nama: false,
      is_error_kelamin: false,
      is_error_alamat: false,
      is_error_telepon: false
    })
    let isPermitted = true
    if(_.isEmpty(this.state.nama)) {
      this.setState({ is_error_nama: true })
      isPermitted = false
    }
    if(_.isNil(this.state.kelamin) || this.state.kelamin === 0) {
      this.setState({ is_error_kelamin: true })
      isPermitted = false
    }
    if(_.isEmpty(this.state.alamat)) {
      this.setState({ is_error_alamat: true })
      isPermitted = false
    }
    if(_.isEmpty(this.state.telepon)) {
      this.setState({ is_error_telepon: true })
      isPermitted = false
    }
    return isPermitted
  }

  _onEditSuccess() {
    this.props.navigation.goBack()
  }

  // update redux yg nyimpan data user
  _updateProfil() {
    let temp = Immutable.asMutable(this.props.data_user, {deep: true})
    temp.data['first_name'] = this.state.nama
    temp.data['gender'] = (this.state.kelamin === 0) ? 'L' : 'P'
    temp.data['address'] = this.state.alamat
    temp.data['phone'] = this.state.telepon
    this.props.updateBio(temp)
  }

  render () {
    return (
      <View style={{flexGrow:1, backgroundColor:'#fff', paddingTop:20}}>

        <View style={{flexGrow:1}}>
          <View style={{flex:10}}>
            <ScrollView
              style={stylesCommon.detailContainer}
              showsVerticalScrollIndicator={false}>
              <View style={{flexGrow:1, flex:1, flexDirection:'column'}}>
                <TextInput
                  onChangeText={(nama) => this.setState({nama})}
                  value={this.state.nama}
                  style={[ styles.inputNormal, {borderColor: this.state.is_error_nama ? 'red' : '#ddd'} ]}
                  placeholder={'Nama'}
                  underlineColorAndroid='rgba(0,0,0,0)' />
                <View style={[ styles.inputNormal, {borderWidth:0, height:65} ]}>
                  <Text style={{marginBottom:4}}>Jenis Kelamin</Text>
                  <View style={{flexDirection:'row'}}>
                    <RadioButton
                      animation={'bounceIn'}
                      isSelected={this.state.kelamin === 0}
                      innerColor={'#333'}
                      outerColor={'#333'}
                      styleContainer={{marginHorizontal:5}}
                      onPress={() => this.setState({kelamin:0})} />
                    <Text style={{alignSelf:'center'}}>Laki-Laki</Text>
                    <RadioButton
                      animation={'bounceIn'}
                      isSelected={this.state.kelamin === 1}
                      innerColor={'#333'}
                      outerColor={'#333'}
                      styleContainer={{marginHorizontal:5}}
                      onPress={() => this.setState({kelamin:1})} />
                    <Text style={{alignSelf:'center'}}>Perempuan</Text>
                  </View>
                </View>
                <TextInput
                  onChangeText={(telepon) => this.setState({telepon})}
                  value={this.state.telepon}
                  style={[ styles.inputNormal, {borderColor: this.state.is_error_telepon ? 'red' : '#ddd'} ]}
                  placeholder={'Telepon'}
                  keyboardType='phone-pad'
                  underlineColorAndroid='rgba(0,0,0,0)' />
                <TextInput
                  onChangeText={(alamat) => this.setState({alamat})}
                  value={this.state.alamat}
                  style={[ styles.inputNormal, {borderColor: this.state.is_error_alamat ? 'red' : '#ddd'} ]}
                  placeholder={'Alamat'}
                  underlineColorAndroid='rgba(0,0,0,0)' />
              </View>
            </ScrollView>
          </View>

          <View style={stylesCommon.buttonDoubleBottomContainer}>
            <ButtonRoundBlue
              onPress={() => this.props.navigation.goBack()}
              style={css.buttonCancel}
              text='Cancel'
              styleText={{color:Colors.buttonDisabledTitle}}/>
            <ButtonRoundBlue
              onPress={() => this._onEditBio()}
              style={css.buttonOk}
              text='OK'/>
          </View>
        </View>

        <ModalLoading
          title='Merubah Profil Anda'
          isFetching={this.props.is_fetching}
          responseOk={this.props.data_bio}
          responseFail={this.props.error_info}
          onConfirmResponseOk={() => this._onEditSuccess()}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data_user: state.user.data_user,
    token: state.user.data_user.token,
    data: state.user.data_user.data,
    data_bio: state.transaction.response,
    is_fetching: state.transaction.is_fetching,
    error_info: state.transaction.error_info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadBio: (token, nama, gender, phone, address) => dispatch(TransactionActions.editBio(token, nama, gender, phone, address)),
    updateBio: (data_user) => dispatch(UserActions.updateBio(data_user)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBio)
