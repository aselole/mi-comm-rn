import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clear,
    justifyContent: 'center',
  },
  modalContainer: {
    margin: 0,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: -5
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  commContainer: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 15,
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  bioContainer: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 15,
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  commItemContainer: {
    borderRadius: 2,
    height: 100,
    width: 250,
    marginHorizontal: 10,
    marginVertical: 20,
    padding: 5,
    flexDirection: 'row',
    backgroundColor:'white',
    elevation: 5,
    shadowOffset: {
      width: 2,
      height: -2
    },
    shadowRadius: 10,
    shadowOpacity: 0.4,
    shadowColor: 'gray',
  },
  commItemSubContainer: {
    flex:1,
    backgroundColor:'white',
    flexDirection:'row'
  },
  "detailPanel": {
    "height": 300,
    "flex": 1,
    "alignItems": "center",
    "justifyContent": "flex-end",
    backgroundColor: 'yellow'
  },
  "detailBlock": {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "rgba(255,255,255,0.8)",
    padding: 10,
    width: width,
    height: 150,
    marginTop: -150,
  },
  "detailName": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 18,
    "paddingTop": 8
  },
  detailDescBlock: {
    padding: 20,
  },
  "detailDesc": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 13,
    "textAlign": "center",
  },
  "detailPrice": {
    "fontSize": 18,
    "textAlign": "center"
  },
  "detailFullPrice": {
    "fontSize": 15,
  },
  imageContainer: {
    width: width / 4,
    height: vh * 15,
    borderRadius: width / 8,
    borderWidth: 1,
    borderColor: '#ddd',
    alignSelf: 'center'
  },
  detailImage: {
    width: width / 4,
    height: vh * 15,
    borderRadius: width / 8,
    borderWidth: 1,
    borderColor: '#eee',
    position: 'relative',
    backgroundColor:'transparent',
  },
  "body": {
      "backgroundColor": "#F2F2F2",
      "paddingBottom": 20
  },
  "card": {
      "width": width,
      "borderBottomWidth": 1,
      "borderBottomColor": "#eee",
      "marginTop": 18,
      "paddingBottom": 6,
      "backgroundColor": "white",
      "position": "relative"
  },
  "row": {
      "flex": 1,
      "flexDirection": "row",
      "justifyContent": "space-between",
      "alignItems": "center",
      "width": width-8,
      "marginTop": 4,
      "marginRight": 4,
      "marginBottom": 4,
      "marginLeft": 4,
      "position": "relative"
  },
  "linebreak": {
      "borderTopWidth": 1,
      "borderTopColor": "#eee",
      "marginTop": 8
  },
  "header": {
      "fontWeight": "500",
      "fontSize": 14,
      "color": "#333"
  },
  "hide": {
      "color": "white"
  },
  "link": {
      "width": width/4 - 30,
      "textAlign": "right",
      "marginRight": 30,
      "fontSize": 12,
      "color": "#bbb"
  },
  "label": {
      "width": 3*width/4 - 40,
      "marginLeft": 30,
      "marginTop": 12,
      "paddingBottom": 12,
      "fontSize": 12,
      "color": "#666",
      "position": "relative"
  },
  "total": {
      "fontSize": 14,
      "color": "#000"
  },
  "labelView": {
      "width": 3*width/4 - 10,
      "paddingLeft": 30,
      "paddingTop": 6,
      "paddingBottom": 6
  },
  "subLabel": {
      "fontSize": 14,
      "color": "#333"
  },
  "fullWidth": {
      "width": width-40
  },
  "description": {
      "fontSize": 12,
      "color": "#aaa",
      "width": width,
      "marginTop": 4
  },
  "value": {
      "width": width/4 - 30 ,
      "textAlign": "right",
      "marginRight": 30,
      "fontSize": 12,
      "fontWeight": "500"
  },
  input: {
    "height": 40,
    "borderColor": "#ddd",
    "borderWidth": 1,
    "fontSize": 14,
    "borderRadius": 4,
    "padding": 4,
    paddingLeft: 12,
    "marginHorizontal": 20,
    "marginBottom": 2,
    "color": "#333",
    fontFamily: Fonts.type.base,
  },
  inputNormal: {
    "height": 40,
    "borderColor": "#ddd",
    "borderWidth": 1,
    "fontSize": 14,
    "borderRadius": 4,
    "padding": 4,
    paddingLeft: 12,
    "marginHorizontal": 20,
    "marginBottom": 8,
    "color": "#333",
    fontFamily: Fonts.type.base,
  },
  inputError: {
    marginBottom: 6,
    marginHorizontal: 20,
    color: 'red',
    fontFamily: Fonts.type.base,
  }
})
