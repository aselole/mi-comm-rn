import React, { Component } from 'react'
import { StyleSheet, View, StatusBar } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'

class RootContainer extends Component {
  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='dark-content' backgroundColor='#fff' />
        { this.props.is_app_ready && <ReduxNavigation /> }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  applicationView: {
    flex: 1
  }
})

const mapStateToProps = (state) => {
  return {
    is_app_ready: state.startup.startedup,
  }
}

export default connect(mapStateToProps, null)(RootContainer)
