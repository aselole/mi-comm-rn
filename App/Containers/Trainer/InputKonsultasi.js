import React, { Component } from 'react'
import { Picker, KeyboardAvoidingView, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import {
  TextInputIcon, NotifDialog, LoadingOverlay, ButtonIcon,
  ImageLoading, ConfirmDialog, ModalLoading, ImagePicker,
  Text, TextArea, ButtonRoundBlue, ButtonRound
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { convertError } from '../../Lib/AppCustomUtils'
import Modal from 'react-native-modal'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class InputKonsultasi extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Isi Konsultasi',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      judul: '',
      isi: '',
      is_empty_judul: false,
      is_empty_isi: false,
    }
  }

  // kirim issue baru ke server
  _onSubmitIssue() {
    if(this._isValid()) {
      this.props.newChatRoom(
        this.props.token,
        this.props.id,
        this.state.judul,
        this.state.isi
      )
    }
  }

  // cek apa isi atau judul sudah diisi sebelum submit konsultasi
  _isValid() {
    this.setState({ is_empty_judul: false, is_empty_isi: false })
    let isValid = true
    if(_.isEmpty(this.state.judul)) {
      this.setState({ is_empty_judul: true })
      isValid = false
    }
    if(_.isEmpty(this.state.isi)) {
      this.setState({ is_empty_isi: true })
      isValid = false
    }
    return isValid
  }

  // konfirmasi sukses kirim issue baru
  _onConfirmSuccess() {
    this.props.onListClicked(this.props.response.id)
    this.props.navigation.navigate('ObrolanChat')
  }

  render () {
    return (
      <View style={{flexGrow:1, backgroundColor:'#fff', paddingTop:20}}>

        <View style={{flexGrow:1}}>
          <View style={{flex:10}}>
            <ScrollView
              style={stylesCommon.detailContainer}
              showsVerticalScrollIndicator={false}>
              <View style={{flexGrow:1, flex:1, flexDirection:'column'}}>
                <TextInput
                  onChangeText={(judul) => this.setState({judul})}
                  value={this.state.judul}
                  style={[ styles.input, {borderColor: this.state.is_empty_judul ? 'red' : '#ddd'} ]}
                  placeholder={'Judul'}
                  underlineColorAndroid='rgba(0,0,0,0)' />
                <TextArea
                  styleContainer={{borderColor: this.state.is_empty_isi ? 'red' : '#ddd'}}
                  title='Isi'
                  onChangeText={(isi) => this.setState({isi})}
                  text={this.state.isi}
                  maxLength={500}/>
              </View>
            </ScrollView>
          </View>

          <View style={styles.buttonContainer}>
            <ButtonRoundBlue
              onPress={() => this.props.navigation.goBack()}
              style={css.buttonCancel}
              text='Cancel'
              styleText={{color:Colors.buttonDisabledTitle}}/>
            <ButtonRoundBlue
              onPress={() => this._onSubmitIssue()}
              style={css.buttonOk}
              text='OK'/>
          </View>
       </View>

       <ModalLoading
         title={this.props.msg_loading}
         isVisible={true}
         isVisibleOk={false}
         isFetching={this.props.is_fetching}
         responseOk={this.props.response}
         responseFail={this.props.error_info}
         onLoadingFinished={() => this._onConfirmSuccess()}
       />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data_user: state.user.data_user.data,
    id: state.list.id,
    msg_loading: state.transaction.msg_loading,
    is_fetching: state.transaction.is_fetching,
    error_info: state.transaction.error_info,
    response: state.transaction.response,
    command: state.transaction.command,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    newChatRoom: (token, user_trainer_id, judul, body) => dispatch(TransactionActions.newChatRoom(token, user_trainer_id, judul, body)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputKonsultasi)
