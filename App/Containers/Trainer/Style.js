import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clear,
    justifyContent: 'center',
  },
  logo: {
    width: deviceWidth / 1.5,
    alignSelf: 'center',
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  input: {
    "height": 40,
    "borderColor": "#ddd",
    "borderWidth": 1,
    "fontSize": 14,
    "borderRadius": 4,
    "padding": 4,
    paddingLeft: 12,
    "marginHorizontal": 20,
    "marginBottom": 8,
    "color": "#333",
    fontFamily: Fonts.type.base,
  },
  textRegisterTitle: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    marginTop: 15,
  },
  textRegister: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  textForgotPass: {
    textAlign: 'right', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  dropdown: {
    height:50,
    width:null,
    borderColor: 'lightgray',
    borderRadius: 2,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  dialogContainer: {
    flex: 0.6,
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    flexDirection: 'column',
    elevation: 12
  },
  dialogTitle: {
    flex: 0.15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  dialogInputContainer: {
    flex: 0.7,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  dialogInputTextShort: {
    flex: 0.25,
    paddingTop: 4,
  },
  dialogInputTextLong: {
    flex: 0.75,
    paddingTop: 4,
  },
  dialogInputBorder: {
    marginTop: 10,
    height: 1,
    backgroundColor: 'red',
  },
  dialogButtonContainer: {
    flex: 0.15,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  dialogButton: {
    flex:1,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    elevation: 7,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  listItemContainer: {
    width: width / 2 - 12,
    height: vh * 26,
    backgroundColor: "#f9f9f9",
    marginLeft: 8,
    marginBottom: 8,
    borderWidth: 1,
    borderColor: "#eee",
    flex: 1,
  },
  listItemImageContainer: {
    flex: 3,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  listItemImageCircle: {
    width: width / 4,
    height: vh * 15,
    borderRadius: width / 8,
    borderWidth: 1,
    borderColor: '#ddd',
  },
  listItemImage: {
    width: width / 4,
    height: vh * 15,
    borderRadius: width / 8,
    borderWidth: 1,
    borderColor: '#eee',
    position: 'relative',
    backgroundColor:'transparent',
  },
  listItemTitle: {
    textAlign: 'center',
    color: '#888',
    fontWeight: '600',
    fontSize: 12,
    fontFamily: Fonts.type.base,
  },
  "detailPanel": {
    "height": 300,
    "flex": 1,
    "alignItems": "center",
    "justifyContent": "flex-end",
    backgroundColor: 'yellow'
  },
  "detailBlock": {
    "alignItems": "center",
    justifyContent: 'center',
    "backgroundColor": "rgba(255,255,255,0.8)",
    //backgroundColor: 'yellow',
    padding: 20,
    width: width,
    height: 100,
    marginTop: -100,
  },
  "detailName": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 18,
    "paddingTop": 8
  },
  detailDescBlock: {
    padding: 20,
    justifyContent:'center',
    alignItems:'center'
  },
  "detailDesc": {
    "color": "#535353",
    "fontWeight": "300",
    "fontSize": 13,
    "textAlign": "center",
  },
  "detailPrice": {
    "fontSize": 18,
    "width": 100,
    "textAlign": "center"
  },
  "detailFullPrice": {
    "fontSize": 15,
  },
  detailImage: {
    width: width,
    height: 400,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingBottom: 16,
  }
})
