import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import TransactionActions from '../../Redux/TransactionRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { DotIndicator } from 'react-native-indicators'
import {
  TextInputIcon, ButtonIcon, ImageLoading,
  ImageFitLoading, ModalLoading, Text
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import { isActiveScreen } from '../../Lib/AppCustomUtils'
import Modal from 'react-native-modal'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import stylesCommon from '../../Themes/StyleCommon'
import css from '../../Themes/Style'

class TrainerDetail extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Trainer',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      data: null
    }
  }

  componentDidMount() {
    this.setState({ data: this.props.data })
  }

  // tampilkan hal input konsultasi
  _onShowInputKonsultasi() {
    this.props.navigation.navigate('InputKonsultasi')
  }

  render () {
    return (
      <View style={{flexGrow:1, backgroundColor:'#fff'}}>
        { !_.isNil(this.state.data) &&
          <View style={{flexGrow:1}}>
            <View style={{flex:10}}>
              <ScrollView
                style={stylesCommon.detailContainer}
                showsVerticalScrollIndicator={false}>
                <ImageFitLoading
                  name={this.state.data.photo}
                  urlDefault={AppConfig.defaultPhotoURL}
                  urlBase={AppConfig.trainerPhotoURL}
                  path={this.state.data.photo}
                  style={styles.detailImage} />
                <View style={styles.detailBlock}>
                  <Text style={styles.detailPrice}>{this.state.data.first_name}</Text>
                  <Text style={styles.detailFullPrice}>{this.state.data.email}</Text>
                </View>
                <View style={styles.detailDescBlock}>
                  <Text style={styles.detailDesc}>{this.state.data.description}</Text>
                </View>
              </ScrollView>

            </View>

            <ButtonIcon
              title='KONSULTASI'
              onPress={() => this._onShowInputKonsultasi()}
              bgColor={Colors.buttonAction}
              styleContainer={stylesCommon.buttonBottomContainer}
              style={stylesCommon.buttonBottom}
              titleColor={Colors.buttonActionTitle}
            />
          </View>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    data: state.list.detail,
    id: state.list.id,
    is_fetching_konsul: state.transaction.is_fetching,
    response_konsul: state.transaction.response,
    error_info_konsul: state.transaction.error_info,
    msg_loading: state.transaction.msg_loading,
    active_screen: state.screen.active,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (token, id) => dispatch(ListActions.getTrainerDetail(token, id)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    newChatRoom: (token, user_trainer_id, judul, body) => dispatch(TransactionActions.newChatRoom(token, user_trainer_id, judul, body)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TrainerDetail)
