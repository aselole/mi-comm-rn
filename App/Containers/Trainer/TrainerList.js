import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import ListActions from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import {
  TextInputIcon, ListGridFilter, ButtonBox, NotifDialog,
  ListFilter, ImageFitLoading, ImageLoading, BoxDashboard
} from '../../Components/'
import AppConfig from '../../Config/AppConfig'
import FlexImage from 'react-native-flex-image'
import { convertError } from '../../Lib/AppCustomUtils'
import { parseHTML } from '../../Lib/HTMLUtils'
import GridView from 'react-native-super-grid'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'
import css from '../../Themes/Style'

class TrainerList extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Trainer',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      error_info: '',
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getData(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ error_info: convertError(nextProps.error_info) })
    }
  }

  _onListClicked(item) {
    this.props.onListClicked(item.id)
    this.props.onDetailReceived(item)
    this.props.navigation.navigate('TrainerDetail')
  }

  _renderList = ({ item }) => {
    return (
      <View style={styles.listItemContainer}>
        <TouchableOpacity
          style={{flex:1, alignItems:'stretch'}}
          onPress={() => this._onListClicked(item)}>
          <View style={styles.listItemImageContainer}>
            <View style={styles.listItemImageCircle}>
              <Image
                source={{uri: AppConfig.trainerPhotoURL+item.photo}}
                style={styles.listItemImage}
              />
            </View>
          </View>
          <View style={{flex:1, justifyContent:'center', backgroundColor:'transparent'}}>
            <Text style={styles.listItemTitle}>
              {_.upperFirst(_.toLower(item.first_name))}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render () {
    return (
      <View style={{flex:1, backgroundColor:'#fff'}}>
        <ListFilter
          data={this.props.data}
          errorInfo={this.state.error_info}
          errorTitle={this.props.error_title}
          isFetching={this.props.is_fetching}
          renderRow={this._renderList.bind(this)}
          loadingType='pacman'
          style={{flexWrap: 'wrap'}}
          styleContent={{flexDirection:'row', flexWrap: 'wrap'}}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.user.data_user.token,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
    data: state.list.data,
    is_fetching: state.list.is_fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getData: (token) => dispatch(ListActions.getTrainer(token)),
    onDetailReceived: (detail) => dispatch(ListActions.onDetailReceived(detail)),
    onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
    resetList: () => dispatch(ListActions.resetList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TrainerList)
