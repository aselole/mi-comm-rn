import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { TextInputIcon, ButtonBox } from '../../Components/'
import FlexImage from 'react-native-flex-image'
import styles from './Style'
var _ = require('lodash');

class Tentang extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
  }

  componentDidMount() {
    setTimeout(() => this._startApp(), 1000)    // pause selama satu detik
  }

  // selesai splash, pindah ke dashboard atau signin
  _startApp() {
    if(!_.isNull(this.props.signed_in) && this.props.signed_in) {
      this.props.navigation.navigate('Dashboard')
    } else {
      this.props.navigation.navigate('Home')
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <FlexImage
          style={styles.logo}
          source={Images.logo} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    signed_in: state.user.ok_signin,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tentang)
