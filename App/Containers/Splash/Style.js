import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  logo: {
    flexGrow: 0.8,
    alignSelf: 'center',
  }
})
