import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import ListAction from '../../Redux/ListRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { TextInputIcon, ButtonIcon, TextTouchable, Text, ErrorPopup } from '../../Components/'
import { convertError, isActiveScreen } from '../../Lib/AppCustomUtils'
import { isAndroid } from '../../Lib/PlatformUtils'
import FCM from 'react-native-fcm'
var _ = require('lodash');

// Styles
import styles from './Style'
import css from '../../Themes/Style'

class Signin extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
    this.state = {
      username: '', password: '',
      error: false, error_info: null,
      error_username: null, error_password: null,
      token_firebase: '1234',
      is_fetching: false,
    }
    this.passTextInput = null
  }

  componentDidMount() {
    // dapatkan token fcm untuk sesi ini
    if(isAndroid()) {
      FCM.getFCMToken().then(token => {
        this.setState({ token_firebase: token || '' });
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!isActiveScreen(nextProps, 'Home')) return
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ is_fetching: false })
      this._onSigninFail(nextProps.error_info)
    }
    if(!_.isEqual(this.props.ok_signin, nextProps.ok_signin) && nextProps.ok_signin) {
      this.props.getCommunity(nextProps.data_user.token)
    }
    if(!_.isEqual(this.props.data_community, nextProps.data_community) && !_.isNull(nextProps.data_community)) {
      this._updateCommunityData(nextProps.data_community)
    }
    if(!_.isEqual(this.props.error_info_community, nextProps.error_info_community) && !_.isNull(nextProps.error_info_community)) {
      this.setState({ is_fetching: false })
      this._onSigninFail(nextProps.error_info_community)
    }
  }

  // update info komunitas pada data user
  _updateCommunityData(item) {
    if(item.hasOwnProperty('community')) {
      let newData = JSON.parse(JSON.stringify(this.props.data_user))
      _.set(newData, 'data.community_id', item.community.id)
      _.set(newData, 'data.community_nama', item.community.nama)
      this.props.updateUser(newData)
    }
    this.setState({is_fetching: false, error_info: null})
    this.props.navigation.navigate('Dashboard')
  }

  // user gagal signin
  _onSigninFail(error) {
    this.setState({error_info: convertError(error)})
  }

  _onSignin() {
    this.setState({ error_info: null })
    let isError = false
    if(_.isEmpty(this.state.username)) {
      this.setState({ error_username: 'Email harus diisi' })
      isError = true
    }
    if(_.isEmpty(this.state.password)) {
      this.setState({ error_password: 'Password harus diisi' })
      isError = true
    }
    if(!isError) {
      this.setState({ is_fetching: true })
      this.props.signin(this.state.username, this.state.password, this.state.token_firebase)
    }
  }

  render () {
    return (
      <View style={styles.layout}>
        <View style={styles.subContainer}>
          <TextInputIcon
            placeholder='E-mail'
            image={Images.user}
            error={this.state.error_username}
            onChange={(username) => this.setState({username})}
            returnKeyType = {"next"}
            onSubmitEditing={(event) => this.passTextInput.focus()} />
          <TextInputIcon
            placeholder='Password'
            image={Images.password}
            error={this.state.error_password}
            onChange={(password) => this.setState({password})}
            secureTextEntry={true}
            inputRef={(input) => this.passTextInput = input} />
          <ButtonIcon
            isLoading={this.state.is_fetching}
            title='Enter'
            onPress={() => this._onSignin()}
            bgColor="rgba(60,135,217,0.8)"
            titleColor='#fff'
            style={{flexGrow:1}}
            styleContainer={{marginTop:18, flexDirection:'row'}} />
        </View>
        <ErrorPopup
          isVisible={_.isEmpty(this.state.error_info) ? false : true}
          message={this.state.error_info}
          styleContainer={{flex:0.3}}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error_info: state.user.error_info_signin,
    ok_signin: state.user.ok_signin,
    data_user: state.user.data_user,
    is_fetching: state.user.fetching_signin,
    data_community: state.list.data,
    error_info_community: state.list.info,
    active_screen: state.screen.active,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signin: (username, password, token_firebase) => dispatch(UserAction.signin(username, password, token_firebase)),
    updateUser: (data_user) => dispatch(UserAction.updateBio(data_user)),
    getCommunity: (token) => dispatch(ListAction.getCommunityMember(token)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signin)
