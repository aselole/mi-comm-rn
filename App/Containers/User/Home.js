import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings, Fonts } from '../../Themes/'
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view"
import { ImageFitLoading, Text } from '../../Components'
import SignIn from './Signin'
import SignUp from './Signup'

// Styles
import styles from './Style'
import css from '../../Themes/Style'

class Home extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
    this.state = {
      active_index: 0
    }
  }

  render () {
    return (
      <View style={css.layout}>
        <View style={css.toolbarMenu}>
          <View style={css.logoContainer}>
            <Text style={{fontSize:Fonts.size.h5, alignSelf:'center'}}>Mi-Comm</Text>
          </View>
        </View>
        <ScrollableTabView
            initialPage={0}
            locked={false}
            tabBarUnderlineStyle={{height: 2, backgroundColor: "#1CAADE"}}
            tabBarActiveTextColor={"#393838"}
            tabBarInactiveTextColor={"#B8B8B8"}
            tabBarTextStyle={{fontFamily:'HelveticaNeue', fontWeight: 'normal', fontSize: 14}}
            style={{backgroundColor: '#ffff'}}
            contentProps={{backgroundColor: '#ffff', marginTop: 0}}
            renderTabBar={() => <ScrollableTabBar
              underlineHeight={3}
              style={{borderBottomColor: '#eee'}}
              tabsContainerStyle={{paddingLeft: 30, paddingRight: 30}}
              tabStyle={{paddingBottom: 0, borderBottomWidth: 0, paddingTop: 0, paddingLeft: 50, paddingRight: 50}}
            />}
            onChangeTab={(tab) => this.setState({ active_index: tab.i}) }
          >
            <SignIn tabLabel="Login" navigation={this.props.navigation} tabIndex={this.state.active_index} />
            <SignUp tabLabel="Sign Up" navigation={this.props.navigation} tabIndex={this.state.active_index} />
          </ScrollableTabView>
        </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error_info: state.user.error_info,
    ok_signin: state.user.ok_signin,
    data_user: state.user.data_user,
    is_fetching: state.user.fetching,
    data_community: state.list.data,
    error_info_community: state.list.info,
    active_screen: state.screen.active,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signin: (username, password, token_firebase) => dispatch(UserAction.signin(username, password, token_firebase)),
    updateUser: (data_user) => dispatch(UserAction.updateBio(data_user)),
    getCommunity: (token) => dispatch(ListAction.getCommunityMember(token)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
