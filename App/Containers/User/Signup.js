import React, { Component } from 'react'
import { Picker, ScrollView, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import {
  TextInputIcon, ButtonIcon, NotifDialog, TextTouchable,
  Text, ErrorPopup
} from '../../Components/'
import call from 'react-native-phone-call'
import { convertError, isActiveScreen } from '../../Lib/AppCustomUtils'
var _ = require('lodash');

// Styles
import styles from './Style'
import css from '../../Themes/Style'

class Signup extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
    this.state = {
      name: '', email: '', password: '',
      error: false, error_info: null,
      response: null,
      error_email: false, error_name: false, error_password: false,
      ok_signup: false,
    }
    this.passEmail = null
    this.passPasswordRep = null
  }

  componentWillReceiveProps(nextProps) {
    if(!isActiveScreen(nextProps, 'Home')) return
    if(!_.isEqual(this.props.error_info, nextProps.error_info) && !_.isNull(nextProps.error_info)) {
      this.setState({ error_info: convertError(nextProps.error_info) })
    }
    if(!_.isEqual(this.props.response, nextProps.response) && !_.isNull(nextProps.response)) {
      this.setState({ response: convertError(nextProps.response) })
    }
    if(!_.isEqual(this.props.ok_signup, nextProps.ok_signup) && nextProps.ok_signup) {
      this._onSignupOk()
    }
  }

  _onSignup() {
    let isError = false
    this.setState({ error_info: '' })
    if(_.isEmpty(this.state.name)) {
      this.setState({ error_name: 'Nama tidak boleh kosong!' })
      isError = true
    }
    else this.setState({ error_name: null })
    if(_.isEmpty(this.state.email)) {
      this.setState({ error_email: 'Email tidak boleh kosong!' })
      isError = true
    }
    else this.setState({ error_email: null })
    if(_.isEmpty(this.state.password)) {
      this.setState({ error_password: 'Password tidak boleh kosong!' })
      isError = true
    }
    else this.setState({ error_password: null })
    if(!isError) {
      this.props.signup(this.state.email, this.state.password, this.state.name)
    }
  }

  _onSignupOk() {
    this.setState({
      ok_signup: true,
      error_info: '',
    })
  }

  _onOkConfirm() {
    this.setState({ok_signup: false})
    this.props.navigation.navigate('Signin')
  }

  _onForgotPassword() {
    const args = {
      number: '082112567388', // String value with the number to call
      prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call
    }
    call(args).catch(console.error)
  }

  render () {
    return (
      <View style={styles.layout}>
        <ScrollView style={styles.subContainer}>
          <TextInputIcon
            placeholder='Full Name'
            onChangeText={(name) => this.setState({name})}
            error={this.state.error_name}
            image={Images.user}
            returnKeyType = {"next"}
            onSubmitEditing={(event) => this.passEmail.focus()}
          />
          <TextInputIcon
            placeholder='E-mail'
            onChangeText={(email) => this.setState({email})}
            error={this.state.error_email}
            image={Images.email}
            returnKeyType = {"next"}
            inputRef={(input) => this.passEmail = input}
            onSubmitEditing={(event) => this.passPasswordRep.focus()}
          />
          <TextInputIcon
            placeholder='Password'
            onChangeText={(password) => this.setState({password})}
            error={this.state.error_password}
            image={Images.password}
            secureTextEntry={true}
            inputRef={(input) => this.passPasswordRep = input}
          />
          <ButtonIcon
            isLoading={this.props.is_fetching}
            title='Registration'
            onPress={() => this._onSignup()}
            bgColor="rgba(60,135,217,0.8)"
            titleColor='#fff'
            style={{flexGrow:1}}
            styleContainer={{marginTop:18, flexDirection:'row'}}  />
          <TextTouchable
            title='Lupa Password? 082112567388'
            onPress={() => this._onForgotPassword()}
            style={{ alignSelf: 'center', marginTop: 5 }}
          />
        </ScrollView>
        <ErrorPopup
          isVisible={_.isEmpty(this.state.error_info) ? false : true}
          message={this.state.error_info}
          styleContainer={{flex:0.3}}
        />
        <NotifDialog
          isVisible={this.state.ok_signup}
          onPressOk={() => this._onOkConfirm()}
          message={this.state.response}
          isSuccess={true}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ok_signup: state.user.ok_signup,
    error_info: state.user.error_info_signup,
    response: state.user.response_signup,
    is_fetching: state.user.fetching_signup,
    active_screen: state.screen.active,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signup: (email, password, name) => dispatch(UserAction.signup(email, password, name)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup)
