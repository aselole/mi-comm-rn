import { StyleSheet, Dimensions } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import stylesTheme, { deviceHeight, deviceWidth, NAV_HEIGHT } from '../../Themes/StyleSub'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  toolbar: {
    backgroundColor: Colors.bgThemeDark,
    height: 93,
    flexDirection: 'column',
    alignItems: 'stretch',
    elevation: 10,
    shadowOffset: {
      width: 5,
      height: 5
    },
    shadowRadius: 4,
    shadowOpacity: 0.2,
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  clusterContainerSmall: {
    width: 30,
    height: 30,
    padding: 6,
    borderWidth: 1,
    borderRadius: 15,
    alignItems: 'center',
    borderColor: Colors.fire,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  clusterTextSmall: {
    fontSize: 12,
    color: Colors.fire,
    fontWeight: '500',
    textAlign: 'center',
  },
  clusterContainerMedium: {
    width: 35,
    height: 35,
    padding: 6,
    borderWidth: 1,
    borderRadius: 20,
    alignItems: 'center',
    borderColor: Colors.fire,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  clusterTextMedium: {
    fontSize: 13,
    color: Colors.fire,
    fontWeight: '500',
    textAlign: 'center',
  },
  clusterContainerLarge: {
    width: 40,
    height: 40,
    padding: 6,
    borderWidth: 1,
    borderRadius: 20,
    alignItems: 'center',
    borderColor: Colors.fire,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  clusterTextLarge: {
    fontSize: 13,
    color: Colors.fire,
    fontWeight: '500',
    textAlign: 'center',
  },
  buttonProceed: {
    flex: 1,
    width: deviceWidth,
    backgroundColor: Colors.bgLight,
    position: 'absolute',
    bottom: 0,
    elevation: 10,
    shadowOffset: {
      width: 5,
      height: -5,
    },
    shadowRadius: 4,
    shadowOpacity: 0.2,
  },
  overlayContainer: {
    flex:1,
    flexDirection: 'row',
    height: vh * 19,
    width: deviceWidth - 20,
    marginHorizontal: 10,
    padding: 8,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: '#ddd',
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 15,
    elevation: 0,
    shadowOffset: {
      width: 2,
      height: 3,
    },
    shadowRadius: 4,
    shadowOpacity: 0.2,
  },
  overlayImageContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  overlayDataContainer: {
    flex: 2.2,
    flexDirection: 'column',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  overlayTitle: {
    fontSize: 14,
    color: '#333',
    fontWeight: '400',
    fontFamily: Fonts.type.base,
  },
  overlaySubtitle: {
    fontSize: 12,
    color: "#999",
    marginTop: 6,
    fontFamily: Fonts.type.base,
  },
})
