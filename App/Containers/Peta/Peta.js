import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, Button, Image } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Strings, Images } from '../../Themes/'
import { getReadableWeekdays, getReadableDate } from '../../Lib/DateUtils'
import { getNormalizedWord } from '../../Lib/ManipulationUtils'
import { filterItem, filterData } from '../../Lib/FilterUtils'
import { parseHTML } from '../../Lib/HTMLUtils'
import ListActions from '../../Redux/ListRedux'
import {
  ListFilter, ImageLoading, MapMarker, ErrorPopup, ModalLoading
} from '../../Components'
import styles from './Styles'
import stylesTheme from '../../Themes/StyleSub'
import AppConfig from '../../Config/AppConfig'
import update from 'immutability-helper'
import { Marker, Callout, AnimatedRegion } from 'react-native-maps'
import ClusteredMapView from 'react-native-maps-super-cluster'
import { DotIndicator } from 'react-native-indicators'
var _ = require('lodash');

import css from '../../Themes/Style'

const indonesiaCenterLatitude = -1.529439,
      indonesiaCenterLongitude = 114.450654

class Peta extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Peta',
      headerStyle: css.noShadow,
    }
  };

  constructor (props) {
    super(props)
    this.state = {
      data: [{location: {latitude:'0.123',longitude:'0.213'}}],
      currentCoordinate: new AnimatedRegion({
        latitude: indonesiaCenterLatitude,
        longitude: indonesiaCenterLongitude
      }),
      lat_delta: 12,
      lng_delta: 12,
      error_gps: false,
      selected: null,
    }
  }

  componentDidMount() {
    this.props.resetList()
    this.props.getData(this.props.token)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data !== nextProps.data) && !_.isNull(nextProps.data)) {
      this.setState({ data: this._normalizeData(nextProps.data) }, () => {
        // zoom map agar semua marker tampil
        new Promise((resolve) => setTimeout(resolve, 2000))
        .then( () => {
          this.map.getMapRef.fitToCoordinates(
            this.state.data.map(m => m.location),
            { edgePadding: 10, animated: true }
          )
        })
      })
    }
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  // terpanggil tiap ada lokasi dari gps, update posisi marker pada peta
  _setupLocationListener() {
    this.watchId = navigator.geolocation.getCurrentPosition(
      (position) => {
        if(_.isNull(this.map)) return
        if(this.state.isFirstLocation) {
          this.setState({
            lat_delta: 0.0043, lng_delta: 0.0043,
            isFirstLocation: false,
          }, () => {
            this._moveRegion(this.map, position.coords, this.state.lat_delta, this.state.lng_delta)
          })
        } else {
          this._moveRegion(this.map, position.coords, this.state.lat_delta, this.state.lng_delta)
        }
        this.setState({
          currentCoordinate: new AnimatedRegion({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          })
        })
      },
      (error) => this.setState({ error_gps: true }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000, distanceFilter: 10 },
    )
  }

  // pindah posisi kamera berdasar suatu lokasi
  _moveRegion(map, coordinate, latDelta, lngDelta) {
    if(_.isNull(map)) return
    map.mapview.animateToRegion(getRegionFromCoordinates(coordinate, latDelta, lngDelta))
  }

  _normalizeData(input) {
    return input.map((item) => {
      let locObj = {latitude: item.lattitude, longitude: item.longitude}
      return update(item, {$merge: {location: locObj}} )
    })
  }

  _getContainerStyle(count) {
    if(count < AppConfig.mapSmallClusterSize) return styles.clusterContainerSmall
    else if(count < AppConfig.mapBigClusterSize) return styles.clusterContainerMedium
    else return styles.clusterContainerLarge
  }

  _getTextStyle(count) {
    if(count < AppConfig.mapSmallClusterSize) return styles.clusterTextSmall
    else if(count < AppConfig.mapBigClusterSize) return styles.clusterTextMedium
    else return styles.clusterTextLarge
  }

  _onMarkerPress(event) {
    let selectedId = event.nativeEvent.id
    if(selectedId.includes('cluster')) {
      this.setState({ selected: null })
    } else {
      this.props.data.map((item) => {
        if(item.id === event.nativeEvent.id) {
          this.setState({ selected: item })
        }
      })
    }
  }

  _onDetailClicked() {
    this.props.onListClicked(this.state.selected.id)
    this.props.navigation.navigate('PartnerDetail')
  }

  _renderCluster = (cluster, onPress) => {
    const pointCount = cluster.pointCount,
          coordinate = cluster.coordinate,
          clusterId = cluster.clusterId
    return (
      <Marker identifier={`cluster-${clusterId}`} coordinate={coordinate} onPress={onPress}>
        <View style={this._getContainerStyle(pointCount)}>
          <Text style={this._getTextStyle(pointCount)}>
            {pointCount}
          </Text>
        </View>
      </Marker>
    )
  }

  _renderMarker = (marker) => (
    <MapMarker
      location={marker.location}
      id={marker.id}
      title={marker.nama}
      icon={Images.marker}
    />
  );

  render () {
    return (
      <View style={styles.map}>
        <ClusteredMapView
          ref={map => { this.map = map }}
          style={styles.map}
          data={this.state.data}
          radius={20}
          renderMarker={this._renderMarker}
          renderCluster={this._renderCluster}
          onMarkerPress={(event) => this._onMarkerPress(event)}
          onClusterPress={(clusterId) => {}}
          preserveClusterPressBehavior={false}
          initialRegion={{
            latitude: indonesiaCenterLatitude,
            longitude: indonesiaCenterLongitude,
            latitudeDelta: 12,
            longitudeDelta: 25
          }}>
          { this.state.data &&
            <Marker.Animated
              ref={marker => { this.marker = marker }}
              coordinate={this.state.currentCoordinate}
              pinColor={Colors.bgThemeDark}>
              <Image source={Images.gps} style={{ width: 25, height: 25 }} />
            </Marker.Animated>
          }
        </ClusteredMapView>

        { this.state.selected &&
          <TouchableOpacity
            key={this.state.selected.id}
            style={styles.overlayContainer}
            onPress={() => this._onDetailClicked()}>
            <View style={styles.overlayImageContainer}>
              <ImageLoading
                name={this.state.selected.logo}
                urlDefault={AppConfig.defaultPhotoURL}
                urlBase={AppConfig.partnerPhotoURL}
                path={this.state.selected.logo} />
            </View>
            <View style={styles.overlayDataContainer}>
              <Text style={styles.overlayTitle} numberOfLines={1}>
                {_.upperFirst(_.toLower(this.state.selected.nama))}
              </Text>
              <Text style={styles.overlaySubtitle} numberOfLines={4}>
                {parseHTML(this.state.selected.short_ket)}...
              </Text>
            </View>
          </TouchableOpacity>
        }

        <ErrorPopup
          isVisible={this.state.error_gps}
          message={Strings.gps_error}
          icon={Images.info}
          styleContainer={{flex:0.15}}/>
        <ModalLoading
          title='Mengunduh Lokasi Partner'
          isFetching={this.props.is_fetching}
          responseOk={this.props.data}
          responseFail={this.props.error}
          isShowConfirmation={false}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  token: state.user.data_user.token,
  is_fetching: state.list.is_fetching,
  error: state.list.error_info,
  data: state.list.data,
})

const mapDispatchToProps = (dispatch) => ({
  getData: (token) => dispatch(ListActions.getPartner(token)),
  resetList: () => dispatch(ListActions.resetList()),
  onListClicked: (id) => dispatch(ListActions.onListClicked(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Peta)
