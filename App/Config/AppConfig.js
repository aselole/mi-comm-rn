// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  baseServerURL: "http://idcosci.com/micomm/",
  basePhotoURL: "http://idcosci.com/micomm/uploads/",
  defaultPhotoURL: "http://www.nsm.or.th/images/no-image-available.png",
  partnerPhotoURL: "http://idcosci.com/micomm/uploads/partner/",
  dealPhotoURL: "http://idcosci.com/micomm/uploads/merchant/",
  beritaPhotoURL: "http://idcosci.com/micomm/uploads/news/",
  trainerPhotoURL: "http://idcosci.com/micomm/uploads/user/",
  promoPhotoURL: "http://idcosci.com/micomm/uploads/promo/",
  obrolanPhotoURL: "http://idcosci.com/micomm/uploads/issue/",
  communityPhotoURL: "http://idcosci.com/micomm/uploads/community/",
  beritaShareURL: "http://idcosci.com/micomm/event/read/",

  mapSmallClusterSize: 10,
  mapBigClusterSize: 20,
}
