import { call, put, select } from 'redux-saga/effects'
import ChatActions, * as selectors from '../Redux/ChatRedux'
import { Strings } from '../Themes/'
var _ = require('lodash')

export function * onMessageArrived (action) {
  let messages = yield select(selectors.messages)
  let oldMessage = _.cloneDeep(messages)
  let newMessage = action.new_message

  if(!_.isNil(oldMessage)) {
    let correspondIndex = _.findIndex(oldMessage, (item) => item.id === newMessage.id)
    if(correspondIndex < 0) {   // chat room tidak ditemukan, tambah chat room baru
      let newData = {}
      newData['id'] = newMessage.id
      newData['data'] = []
      newData['data'].push({
        title: newMessage.title,
        body: newMessage.body,
        created_at: newMessage.created_at
      })
      oldMessage.push(newData)
    } else {  // chat room ditemukan, tambah message di chat room yg dituju
      oldMessage[correspondIndex]['data'].push({
        title: newMessage.title,
        body: newMessage.body,
        created_at: newMessage.created_at
      })
    }
    yield put(ChatActions.appendMessage(oldMessage))
  }
}

export function * onMessageRead (action) {
  let chat_room_read_id = yield select(selectors.chat_room_read_id)
  let oldRoomReadId = _.cloneDeep(chat_room_read_id)
  let newRoomReadId = action.new_chat_room_read_id

  oldRoomReadId.push(newRoomReadId)

  yield put(ChatActions.updateMessageRead(oldRoomReadId))
}
