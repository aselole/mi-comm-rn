import { call, put, select } from 'redux-saga/effects'
import UploadActions from '../Redux/UploadRedux'
import TransactionActions from '../Redux/TransactionRedux'
import { Strings } from '../Themes/'
import { getFilenameFromPath, getFileType } from '../Lib/FileUtils'
import { jsonToFormEncodedCaps } from '../Lib/JsonUtils'
var _ = require('lodash')

// upload foto user ke server
export function * uploadFotoUser (api, action) {
  const { data, token } = action
  const form = new FormData()
  if(_.isNull(data) || !_.isArray(data)) return
  form.append('photo', {
    name: getFilenameFromPath(data[0]),
    uri: data[0],
    type: getFileType(data[0]),
  })

  const response = yield call(api.uploadFotoUser, form, token)
  if(response.ok) yield put(UploadActions.onUploadSuccess(response.data))
  else yield put(UploadActions.onUploadFailed(response.data))
}

export function * newIssue (api, action) {
  const { featured_img, token } = action
  const form = new FormData()
  // contoh jika upload attachment dan data string sekaligus
  if(!_.isNil(featured_img)) {
    form.append('featured_img', {
      name: getFilenameFromPath(featured_img),
      uri: featured_img,
      type: getFileType(featured_img),
    })
  }
  form.append('judul', JSON.stringify(action.judul))
  form.append('body', JSON.stringify(action.body))

  const response = yield call(api.newIssue, form, token)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}
