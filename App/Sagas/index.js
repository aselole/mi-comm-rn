import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from '../Services/Api'
import API_ATTACHMENT from '../Services/ApiAttachment'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { UserTypes } from '../Redux/UserRedux'
import { ListTypes } from '../Redux/ListRedux'
import { TransactionTypes } from '../Redux/TransactionRedux'
import { UploadTypes } from '../Redux/UploadRedux'
import { ChatTypes } from '../Redux/ChatRedux'

/* ------------- Sagas ------------- */

import { signin, signup } from './UserSagas'
import {
  getPartner, getPartnerDetail,
  getDeals, getDealsDetail,
  getBerita, getBeritaDetail,
  getTrainer,
  getEventGlobal, getEventMember, getEventDetail,
  getCommunityMember, getObrolanComm, getObrolanCommDetail,
  getObrolanPrivate, getObrolanPrivateDetail
} from './ListSagas'
import {
  takePromo, editBio, editPass,
  newComment, deleteComment, reportComment,
  deleteIssue, newChatMessage, newChatRoom
} from './TransactionSagas'
import {
  uploadFotoUser, newIssue
} from './UploadSagas'
import {
  onMessageArrived, onMessageRead
} from './ChatSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()
const apiAttachment = API_ATTACHMENT.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root() {
  try {
    yield [
      takeLatest(UserTypes.SIGNIN, signin, api),
      takeLatest(UserTypes.SIGNUP, signup, api),

      takeEvery(ListTypes.GET_PARTNER, getPartner, api),
      takeEvery(ListTypes.GET_PARTNER_DETAIL, getPartnerDetail, api),

      takeEvery(ListTypes.GET_DEALS, getDeals, api),
      takeEvery(ListTypes.GET_DEALS_DETAIL, getDealsDetail, api),

      takeEvery(ListTypes.GET_BERITA, getBerita, api),
      takeEvery(ListTypes.GET_BERITA_DETAIL, getBeritaDetail, api),

      takeEvery(ListTypes.GET_TRAINER, getTrainer, api),

      takeEvery(ListTypes.GET_EVENT_GLOBAL, getEventGlobal, api),
      takeEvery(ListTypes.GET_EVENT_MEMBER, getEventMember, api),
      takeEvery(ListTypes.GET_EVENT_DETAIL, getEventDetail, api),

      takeEvery(ListTypes.GET_OBROLAN_COMM, getObrolanComm, api),
      takeEvery(ListTypes.GET_OBROLAN_COMM_DETAIL, getObrolanCommDetail, api),
      takeEvery(ListTypes.GET_OBROLAN_PRIVATE, getObrolanPrivate, api),
      takeEvery(ListTypes.GET_OBROLAN_PRIVATE_DETAIL, getObrolanPrivateDetail, api),

      takeEvery(ListTypes.GET_COMMUNITY_MEMBER, getCommunityMember, api),

      takeEvery(TransactionTypes.TAKE_PROMO, takePromo, api),
      takeEvery(TransactionTypes.EDIT_BIO, editBio, api),
      takeEvery(TransactionTypes.EDIT_PASS, editPass, api),
      takeEvery(TransactionTypes.NEW_COMMENT, newComment, api),
      takeEvery(TransactionTypes.DELETE_COMMENT, deleteComment, api),
      takeEvery(TransactionTypes.REPORT_COMMENT, reportComment, api),
      takeEvery(TransactionTypes.NEW_ISSUE, newIssue, apiAttachment),
      takeEvery(TransactionTypes.DELETE_ISSUE, deleteIssue, api),
      takeEvery(TransactionTypes.NEW_CHAT_MESSAGE, newChatMessage, api),
      takeEvery(TransactionTypes.NEW_CHAT_ROOM, newChatRoom, api),

      takeEvery(ChatTypes.ON_MESSAGE_ARRIVED, onMessageArrived),
      takeEvery(ChatTypes.ON_MESSAGE_READ, onMessageRead),

      takeEvery(UploadTypes.UPLOAD_FOTO_USER, uploadFotoUser, apiAttachment),
    ]
  }
  catch(error) {
    alert('error saga '+error)
  }
}
