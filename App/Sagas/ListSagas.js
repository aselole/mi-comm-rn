import { call, put } from 'redux-saga/effects'
import { Strings } from '../Themes/'
import ListActions from '../Redux/ListRedux'

export function * getPartner (api, action) {
  const response = yield call(api.getPartner, action)
  if (response.ok) {
    yield put(ListActions.onDataReceived(response.data))
  } else {
    yield put(ListActions.onDataError(response))
  }
}

export function * getPartnerDetail (api, action) {
  const response = yield call(api.getPartnerDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}

export function * getDeals (api, action) {
  const response = yield call(api.getDeals, action)
  if (response.ok) {
    yield put(ListActions.onDataReceived(response.data))
  } else {
    yield put(ListActions.onDataError(response))
  }
}

export function * getDealsDetail (api, action) {
  const response = yield call(api.getDealsDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}

export function * getBerita (api, action) {
  const response = yield call(api.getBerita, action)
  if (response.ok) {
    yield put(ListActions.onDataReceived(response.data))
  } else {
    yield put(ListActions.onDataError(response))
  }
}

export function * getBeritaDetail (api, action) {
  const response = yield call(api.getBeritaDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}

export function * getTrainer (api, action) {
  const response = yield call(api.getTrainer, action)
  if (response.ok) {
    yield put(ListActions.onDataReceived(response.data))
  } else {
    yield put(ListActions.onDataError(response))
  }
}

export function * getEventGlobal (api, action) {
  const response = yield call(api.getEventGlobal, action)
  if (response.ok) {
    yield put(ListActions.onArrayDataReceived(response.data, 0))
  } else {
    yield put(ListActions.onArrayDataError(response, 0))
  }
}

export function * getEventMember (api, action) {
  const response = yield call(api.getEventMember, action)
  if (response.ok) {
    yield put(ListActions.onArrayDataReceived(response.data, 1))
  } else {
    yield put(ListActions.onArrayDataError(response, 1))
  }
}

export function * getEventDetail (api, action) {
  const response = yield call(api.getEventDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}

export function * getCommunityMember (api, action) {
  const response = yield call(api.getCommunityMember, action)
  if (response.ok) {
    yield put(ListActions.onDataReceived(response.data))
  } else {
    yield put(ListActions.onDataError(response))
  }
}

export function * getObrolanComm (api, action) {
  const response = yield call(api.getObrolanComm, action)
  if (response.ok) {
    yield put(ListActions.onArrayDataReceived(response.data, 0))
  } else {
    yield put(ListActions.onArrayDataError(response, 0))
  }
}

export function * getObrolanCommDetail (api, action) {
  const response = yield call(api.getObrolanCommDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}

export function * getObrolanPrivate (api, action) {
  const response = yield call(api.getObrolanPrivate, action)
  if (response.ok) {
    yield put(ListActions.onArrayDataReceived(response.data, 1))
  } else {
    yield put(ListActions.onArrayDataError(response, 1))
  }
}

export function * getObrolanPrivateDetail (api, action) {
  const response = yield call(api.getObrolanPrivateDetail, action)
  if (response.ok) {
    yield put(ListActions.onDetailReceived(response.data))
  } else {
    yield put(ListActions.onDetailError(response.data))
  }
}
