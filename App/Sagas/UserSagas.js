import { call, put } from 'redux-saga/effects'
import UserActions from '../Redux/UserRedux'
import { Strings } from '../Themes/'

export function * signin (api, action) {
  const response = yield call(api.signin, action)

  if (response.ok) {
    yield put(UserActions.signinOk(response.data))
  } else {
    yield put(UserActions.signinFail(response.data))
  }
}

export function * signup (api, action) {
  const response = yield call(api.signup, action)

  if (response.ok) {
    yield put(UserActions.signupOk(response.data))
  } else {
    yield put(UserActions.signupFail(response.data))
  }
}

export function * forgotPass (api, action) {
  const response = yield call(api.usernameCheck, action)

  if(response.ok) {
    yield put(UserActions.forgotPassOk(response.data.id_detail, response.data.hp))
  } else {
    yield put(UserActions.forgotPassFail(response))
  }
}

export function * updatePass (api, action) {
  const response = yield call(api.updatePassword, action)

  if(response.ok) {
    yield put(UserActions.updatePassOk())
  } else {
    yield put(UserActions.updatePassFail(response))
  }
}
