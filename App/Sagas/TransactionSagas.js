import { call, put } from 'redux-saga/effects'
import { Strings } from '../Themes/'
import TransactionActions from '../Redux/TransactionRedux'

export function * takePromo (api, action) {
  const response = yield call(api.takePromo, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * editBio (api, action) {
  const response = yield call(api.editBio, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * editPass (api, action) {
  const response = yield call(api.editPass, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * newComment (api, action) {
  const response = yield call(api.newComment, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * deleteComment (api, action) {
  const response = yield call(api.deleteComment, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * reportComment (api, action) {
  const response = yield call(api.reportComment, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * deleteIssue (api, action) {
  const response = yield call(api.deleteIssue, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * newChatMessage (api, action) {
  const response = yield call(api.newChatMessage, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}

export function * newChatRoom (api, action) {
  const response = yield call(api.newChatRoom, action)
  if (response.ok) {
    yield put(TransactionActions.onTransactionReceived(response.data))
  } else {
    yield put(TransactionActions.onTransactionError(response.data))
  }
}
