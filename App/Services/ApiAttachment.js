import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import UploadActions from '../Redux/UploadRedux'
import { store } from '../Containers/App'         // how to access redux from non-react component
import { isNull } from '../Lib/CheckUtils'

// API untuk handle attachment upload
const create = (baseURL = AppConfig.baseServerURL) => {
  const apiAttachment = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'multipart/form-data',
      'Accept': 'application/json'
    },
    timeout: 50000,
    onUploadProgress: progress => {                 // listener untuk progress upload attachment
      // store.dispatch(UploadActions.attachmentUploadProgress(progress.loaded, progress.total, store.getState().upload.upload_sequence[0]))   // how to access redux from non-react component
    }
  })

  apiAttachment.addRequestTransform(request => {  // rubah format request parameter
  })

  apiAttachment.addResponseTransform(response => {  // rubah format response parameter
    if(isNull(response.data)) response.ok = false
    else if(!isNull(response.problem)) {
      response.ok = false
      response.data = response.problem
    }
    else if(!isNull(response.data.success) && !response.data.success) response.ok = false
    else if(!isNull(response.data.code) && response.data.code === 500) response.ok = false
  })

  const uploadFotoUser = (payload, token) => {
    apiAttachment.setHeader('X-API-KEY', token)
    return apiAttachment.post('/api/profile/edit_photo', payload)
  }
  const newIssue = (payload, token) => {
    apiAttachment.setHeader('X-API-KEY', token)
    return apiAttachment.post('/api/issue/store', payload)
  }

  return {
    uploadFotoUser,
    newIssue,
  }
}

export default {
  create
}
