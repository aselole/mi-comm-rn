import apisauce from 'apisauce'
import { jsonToFormEncodedCaps } from '../Lib/JsonUtils'
import AppConfig from '../Config/AppConfig'
import { store } from '../Containers/App'       // how to access redux from non-react component
import { isNull } from '../Lib/CheckUtils'

const create = (baseURL = AppConfig.baseServerURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    timeout: 50000,
    onUploadProgress: progress => {                 // listener untuk progress upload attachment
      // store.dispatch(UploadActions.dataUploadProgress(progress.loaded, progress.total, store.getState().upload.upload_sequence[0]))   // how to access redux from non-react component
    }
  })

  api.addRequestTransform(request => {  // rubah format request parameter
    request.data = jsonToFormEncodedCaps(request.data)
  })

  api.addResponseTransform(response => {  // rubah format response parameter
    if(isNull(response.data)) response.ok = false
    else if(!isNull(response.problem)) {
      response.ok = false
      if(!isNull(response.data) && response.data.success) { // handle list kosong
        response.ok = true
        response.data = response.data
      } else if(isNull(response.data)){
        response.data = response.problem
      }
    }
    else if(!isNull(response.data.success) && !response.data.success) response.ok = false
    else if(!isNull(response.data.code) && response.data.code === 500) response.ok = false
  })

  const signin = (payload) => api.post('api/login/micomm', payload)
  const signup = (payload) => api.post('mic/index.php/user/add', payload)

  const getPartner = (payload) => api.get('api/partner?X-API-KEY='+payload.token)
  const getPartnerDetail = (payload) => api.get(
                              'api/partner?X-API-KEY='+payload.token+'&id='+payload.id )

  const getDeals = (payload) => api.get('api/merchant?X-API-KEY='+payload.token)
  const getDealsDetail = (payload) => api.get(
                              'api/merchant?X-API-KEY='+payload.token+'&id='+payload.id )

  const getBerita = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/news')
  }
  const getBeritaDetail = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/news?id='+payload.id)
  }

  const getTrainer = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/user/list_trainer')
  }

  const getEventGlobal = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/promo/list_promo?is_global=y')
  }
  const getEventMember = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/promo/list_promo?is_global=t')
  }
  const getEventDetail = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/promo/detail?promo_id='+payload.id)
  }

  const takePromo = (payload) => api.post('/api/promo/claim', payload)

  const getCommunityMember = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/community/my_community')
  }

  const editBio = (payload) => api.post('/api/profile/edit_bio', payload)

  const editPass = (payload) => api.post('/api/profile/edit_password', payload)

  const getObrolanComm = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/issue/list')
  }
  const getObrolanCommDetail = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/issue/detail?id='+payload.id)
  }

  const getObrolanPrivate = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/private_issue/list')
  }
  const getObrolanPrivateDetail = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.get('/api/private_issue/detail?id='+payload.id)
  }

  const newComment = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.post('/api/issue/store_comment', payload)
  }
  const deleteComment = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.delete('api/issue/test/'+payload.id)
    // return api.delete('index.php/api/issue/delete_issue_comment', {params: { id: payload.id } })
  }
  const reportComment = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.post('/api/issue/laporkan_comment', payload)
  }

  const deleteIssue = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.delete('/api/issue/delete_issue/'+payload.id)
  }

  const newChatMessage = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.post('/api/private_issue/store_comment', payload)
  }

  const newChatRoom = (payload) => {
    api.setHeader('X-API-KEY', payload.token)
    return api.post('/api/private_issue/store', payload)
  }

  return {
    signin,
    signup,
    getPartner,
    getPartnerDetail,
    getDeals,
    getDealsDetail,
    getBerita,
    getBeritaDetail,
    getTrainer,
    getEventGlobal,
    getEventMember,
    getEventDetail,
    takePromo,
    getCommunityMember,
    editBio,
    editPass,
    getObrolanComm,
    getObrolanCommDetail,
    getObrolanPrivate,
    getObrolanPrivateDetail,
    newComment,
    deleteComment,
    reportComment,
    deleteIssue,
    newChatMessage,
    newChatRoom,
  }
}

export default {
  create
}
