import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import FastImage from 'react-native-fast-image'
import { createImageProgress } from 'react-native-image-progress'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'
import { getFirstParam } from '../Lib/UrlUtils'
import { isLocalImage, isAssetImage } from '../Lib/CheckUtils'
var _ = require('lodash')

const FastLoadingImage = createImageProgress(FastImage)

export default class ImageLoading extends Component {

  constructor(props) {
    super(props)
    this.state = {
      source: null,
      fetching: false,
      finished: false,
      loaded: 0, total: 0
    }
  }

  componentDidMount() {
    this._showMainImage()
  }

  _showMainImage() {
    if(_.isEmpty(this.props.path)) {
      this._showDefaultImage()
      return
    }
    if(isAssetImage(this.props.path)) {
      this.setState({ source: this.props.path })
    } else if(isLocalImage(this.props.path)) {
      this.setState({ source: { uri: this.props.path }})
    } else {
      this.setState({ source:
        { uri: this.props.urlBase + this.props.urlNormalizer(this.props.path) }
      })
    }
  }

  _showDefaultImage() {
    if(_.isNil(this.props.urlDefault)) return
    if(isAssetImage(this.props.urlDefault)) {
      this.setState({ source: this.props.urlDefault })
    } else {
      this.setState({ source: { uri: this.props.urlDefault }})
    }
  }

  _onStartFetching() {
    this.setState({ fetching: true, finished: false })
  }

  _onFetchingProgress(e) {
    this.setState({ loaded: e.nativeEvent.loaded, total: e.nativeEvent.total })
  }

  _onFinishFetching() {
    this.setState({ fetching: false, finished: true })
  }

  _onErrorFetching() {
    this._showDefaultImage()
  }

  render () {
    return (
      <View style={[ this.props.styleContainer ]}>
        <FastLoadingImage
          style={[ this.props.style, { flex:1, position:'absolute',top:0,bottom:0,left:0,right:0 } ]}
          borderRadius={this.props.borderRadius}
          source={this.state.source}
          resizeMode={FastImage.resizeMode.cover}
          indicator={Progress.Pie}
          onLoadStart={() => this._onStartFetching()}
          onProgress={e => this._onFetchingProgress(e)}
          onLoad={() => {}}
          onError={() => this._onErrorFetching()}
          onLoadEnd={() => this._onFinishFetching()} />
      </View>
    )
  }
}

const styles = StyleSheet.create({ });

ImageLoading.propTypes = {
  name: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  path: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.string ]),
  urlBase: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  urlDefault: PropTypes.oneOfType([ PropTypes.object, PropTypes.string, PropTypes.number ]),
  urlNormalizer: PropTypes.func,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  token: PropTypes.string,
  borderRadius: PropTypes.number,
}

ImageLoading.defaultProps = {
  name: 'Foto',
  urlNormalizer: getFirstParam,
  style: { flex: 1 },
  styleContainer: { flex: 1 },
  borderRadius: 1,
}
