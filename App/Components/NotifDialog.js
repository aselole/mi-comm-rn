import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types'
import StyleSub from '../Themes/StyleSub';
import LinearGradient from 'react-native-linear-gradient';
import { Colors, Images, Servers, Strings } from '../Themes/'
import { Dialog } from 'react-native-simple-dialogs'
import Modal from 'react-native-modal'
import FlexImage from 'react-native-flex-image'
import ViewMoreText from 'react-native-view-more-text'
import Collapsible from 'react-native-collapsible'

// Styles
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class NotifDialog extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flexContainer: 0.3,
      flexIconContainer: 1.5,
      flexMessageContainer: 0.1,
      flexButtonContainer: 0.7,
    }
  }

  toggle() {
    this.props.isVisible = !this.props.isVisible
  }

  _onContentChanged(height) {
    if(height > 50) {
      this.setState({
        flexContainer: 1,
        flexIconContainer: 0.15,
        flexMessageContainer: 0.8,
        flexButtonContainer: 0.1,
      })
    }
  }

  render() {
    let indicators = this.props.isSuccess ? Images.ok : Images.fail
    return (
      <Modal
        isVisible={this.props.isVisible}
        backdropColor='gray'
        backdropOpacity={0.5}
        animationInTiming={1000}>
        <View style={[ styles.container, {flex: this.state.flexContainer} ]}>

          <View style={[ styles.iconContainer, {flex: this.state.flexIconContainer} ]}>
            <FlexImage source={indicators} style={styles.icon} />
          </View>

          <ScrollView
            style={[ styles.messageContainer, {flex: this.state.flexMessageContainer} ]}
            contentContainerStyle={styles.messageSubContainer}
            onContentSizeChange={(w, h) => this._onContentChanged(h)}>
            <Text style={[{textAlign: 'center', fontSize: 14}, stylesTheme.mediumBold, stylesTheme.greyColor ]}>
              {this.props.message}
            </Text>
          </ScrollView>

          <View style={[ styles.buttonContainer, {flex: this.state.flexButtonContainer} ]}>
            <TouchableOpacity style={styles.button}
              onPress={this.props.onPressOk}>
              <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, {textAlign: 'center', fontSize:16} ]}>
                OK
              </Text>
            </TouchableOpacity>
          </View>

        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    elevation: 12,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 8,
    shadowOpacity: 0.5,
  },
  iconContainer: {
    backgroundColor: 'transparent',
  },
  icon: {
    alignSelf: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
  },
  messageContainer: {
    flex: 0.1,
    flexDirection:'column',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  messageSubContainer: {
    justifyContent:'center'
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  button: {
    flex: 1,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 12,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 0.5,
  },
});

NotifDialog.propTypes = {
  isVisible: PropTypes.bool,
  onPressOk: PropTypes.func,
  message: PropTypes.string,
  isSuccess: PropTypes.bool,
};

NotifDialog.defaultProps = {
  isSuccess: true
}
