import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import ImageFitLoading from './ImageFitLoading'
import ImageLoading from './ImageLoading'

export default class SwipedView extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    return (
      <View style={this.props.isLeft ? styles.swipeContainerLeft : styles.swipeContainerRight}>
        <View style={this.props.isLeft ? styles.imageContainerLeft : styles.imageContainerRight}>
          <ImageFitLoading
            urlDefault={Images.share}
            style={styles.icon} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  swipeContainerLeft: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#ddd'
  },
  swipeContainerRight: {
    flex: 1,
    alignItems: 'flex-end',
    backgroundColor: '#ddd'
  },
  imageContainerLeft: {
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#eee'
  },
  imageContainerRight: {
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#eee'
  },
  icon: {
    flexGrow: 0.2,
    width: 30,
  }
});

SwipedView.propTypes = {
  icon: PropTypes.number,
  isLeft: PropTypes.bool,
}

SwipedView.defaultProps = {
  isLeft: true,
}
