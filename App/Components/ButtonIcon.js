import React, {Component} from "react"
import {Keyboard, View, StyleSheet, Dimensions, Image, TouchableOpacity} from "react-native"
import PropTypes from 'prop-types'
import { Colors, Images, Servers, Strings, Fonts } from '../Themes/'
import stylesTheme, {shadowOffsetWidth, shadowOffsetHeight, shadowRadius, shadowOpacity} from '../Themes/StyleSub';
const {width} = Dimensions.get("window")
import ConfirmDialog from './ConfirmDialog'
import { DotIndicator } from 'react-native-indicators'  // Loading
import Text from './Text'
var _ = require('lodash')

export default class ButtonIcon extends Component {

  constructor(props) {
    super(props);
    this.state = {
      confirm: 0,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.isLoading, nextProps.isLoading)) {
      Keyboard.dismiss()
    }
  }

  _onPress = () => {
    this.setState((prev) => { return {confirm:prev.confirm+1} })
  }

  render() {
    let onPress = this.props.isNeedConfirm ? this._onPress : this.props.onPress
    let bgColor = this.props.isLoading || this.props.isDisabled ? this.props.loadingBgColor : this.props.bgColor
    let textColor = this.props.isLoading || this.props.isDisabled ? this.props.loadingTitleColor : this.props.titleColor
    return (
      <View style={[this.props.styleContainer]}>
        <TouchableOpacity
          style={[styles.button, this.props.style, {backgroundColor: bgColor}] }
          disabled={this.props.isLoading || this.props.isDisabled}
          onPress={onPress}>
          { !this.props.isLoading &&
            <Text style={[ styles.title, this.props.styleTitle, {color: textColor, fontFamily: Fonts.type.base} ]}>{this.props.title && this.props.title}</Text>
          }
          { !this.props.isLoading && this.props.icon &&
            <Image source={this.props.icon} style={styles.icon} />
          }
          { this.props.isLoading &&
            <DotIndicator color='white' count={4} size={5} style={{alignSelf: 'center', marginVertical: 8}} />
          }
        </TouchableOpacity>

        <ConfirmDialog
          isVisible={this.state.confirm}
          onPressOk={this.props.onPress}
          title={this.props.confirmTitle}
          message={this.props.confirmMessage}
          icon={this.props.confirmIcon} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "rgba(60,135,217,0.8)",
    padding: 12,
    borderColor: "transparent",
    alignSelf: "center",
    borderRadius: 20,
  },
  title: {
    alignSelf: 'center'
  },
  icon: {
    position: 'absolute',
    right: 10,
    width: 30,
    height: 30
  }
})

ButtonIcon.propTypes = {
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  styleTitle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  title: PropTypes.string,
  titleColor: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  loadingTitleColor: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  loadingBgColor: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  bgColor: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
  icon: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
  isNeedConfirm: PropTypes.bool,
  isDisabled: PropTypes.bool,
  confirmTitle: PropTypes.string,
  confirmMessage: PropTypes.string,
  confirmIcon: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
};

ButtonIcon.defaultProps = {
  isLoading: false,
  isDisabled: false,
  isNeedConfirm: false,
  bgColor: Colors.theme,
  loadingBgColor: 'gray',
};
