import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, Dimensions, StyleSheet, View, Text,
  SectionList, ScrollView
} from 'react-native'
import { Colors, Strings, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import stylesTheme from '../Themes/StyleSub'
import { DotIndicator, WaveIndicator, PacmanIndicator } from 'react-native-indicators'
import update from 'immutability-helper'
import { filterData } from '../Lib/FilterUtils'
import GridView from 'react-native-super-grid'
import FlexImage from 'react-native-flex-image'
import CollapseView from 'react-native-collapse-view'
var _ = require('lodash')
var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
/*
 * List data dengan fitur filter
 * metode filter: otomatis filter ketika ada perubahan kata kunci pencarian/parameter)
*/
export default class ListGridFilter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      error_title: false,
      items: null,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.errorTitle, nextProps.errorTitle) && !_.isNull(nextProps.errorTitle)) {
      this.setState({ error_title: nextProps.errorTitle })
    }
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNil(nextProps.data)) {
      this.setState({ items: nextProps.data })
    }
    if(!_.isEqual(this.props.filterParam, nextProps.filterParam) && !_.isNil(nextProps.filterParam)) {
      this._performFilter(nextProps.filterParam)
    }
  }

  // pencarian data
  _performFilter(filters) {
    this.setState({
      items: this.props.data
    }, () => {
      let newData =
      ( this.state.items !== null &&
          this.state.items.filter((item) =>
            filters.every((filter) => {
              if(filter.type === 'dropdown' && filter.value == 'Semua') {         // utk parameter tipe dropdown, jika pilih 'semua' maka tdk perlu filter
                return true
              } else {                                                            // utk parameter tipe lain, periksa apa cocok dgn current data atau tidak
                return item[filter.column].toLowerCase().includes(filter.value.toLowerCase())
              }
            }
        ))
      )
      this.setState(prevState => ({
        items: update(prevState.items, {$set: newData})
      }))
    })
  }

  render () {
    return (
      <View>
        { this.props.isFetching &&
          <View style={{alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
            { this.props.loadingType === 'dot' &&
              <DotIndicator color={Colors.theme} count={4} size={13} style={{}} />
            }
            { this.props.loadingType === 'wave' &&
              <WaveIndicator color={Colors.theme} count={4} size={80} style={{}} />
            }
            { this.props.loadingType === 'pacman' &&
              <PacmanIndicator color={Colors.theme} count={4} size={100} style={{}} />
            }
          </View>
        }
        { !_.isNull(this.state.items) && !this.props.isFetching &&
          <GridView
            items={this.state.items}
            renderItem={this.props.renderRow}
            keyExtractor={(item, index) => item[0].id}
            style={{flex:1, flexGrow:1}}
          />
        }
        { !_.isNull(this.props.errorInfo) && !this.props.isFetching &&
          <View
            style={{height:deviceHeight, alignItems:'center', justifyContent:'center'}}>
            <View style={{flex:0.4, justifyContent:'flex-end'}}>
              <FlexImage
                style={{flex:0.7, alignSelf:'center'}}
                source={this.state.is_empty ? Images.info : Images.fail} />
            </View>
            <View style={{flex:0.6, flexDirection:'column', marginTop:10}}>
              <ScrollView>
                <CollapseView
                  collapse={false}
                  renderView={(collapse) => (
                    <View style={{flexDirection:'row', alignSelf:'center'}}>
                      <Text>{this.state.error_title}</Text>
                      { !_.isEmpty(this.props.errorInfo) &&
                        <Image source={Images.down} style={{marginLeft:5, width:20, height:20}} />
                      }
                    </View>
                  )}
                  renderCollapseView={(collapse) => {
                    if(_.isEmpty(this.props.errorInfo)) return (<View></View>)
                    else return (
                      <View style={{margin:10, padding:8, backgroundColor:'white'}}>
                        <Text>{this.props.errorInfo}</Text>
                      </View>
                    )
                  }}
                />
              </ScrollView>
            </View>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({

});

ListGridFilter.propTypes = {
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  isFetching: PropTypes.bool,
  isDataEmpty: PropTypes.bool,
  isError: PropTypes.bool,
  errorTitle: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
  errorInfo: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
  renderRow: PropTypes.func,
  styleGrid: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  /*
   * parameter pencarian data yg fleksibel
   * update state ini di parent utk filter data scr otomatis
   * format filter => [{column: 'JUDUL OBJECT YG DICARI', type: 'text/dropdown'. value: 'APA YANG DICARI'}]
  */
  filterParam: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ])
}

ListGridFilter.defaultProps = {

}
