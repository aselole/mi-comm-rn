import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, ActivityIndicator, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import FlexImage from 'react-native-flex-image'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'
import { getFirstParam } from '../Lib/UrlUtils'
import { isLocalImage, isAssetImage } from '../Lib/CheckUtils'
import { DotIndicator, WaveIndicator, PulseIndicator, BarIndicator, BallIndicator } from 'react-native-indicators'
var _ = require('lodash')

export default class ImageFitLoading extends Component {

  constructor(props) {
    super(props)
    this.state = {
      source: null,
      fetching: false,
      finished: false,
      loaded: 0, total: 0
    }
  }

  componentWillMount() {
    this._showMainImage()
  }

  _showMainImage() {
    if(_.isEmpty(this.props.path)) {
      this._showDefaultImage()
      return
    }
    if(isAssetImage(this.props.path)) {
      this.setState({ source: this.props.path })
    } else if(isLocalImage(this.props.path)) {
      this.setState({ source: { uri: this.props.path }})
    } else {
      this.setState({ source:
        { uri: this.props.urlBase + this.props.urlNormalizer(this.props.path) }
      })
    }
  }

  _showDefaultImage() {
    if(_.isNil(this.props.urlDefault)) return
    if(isAssetImage(this.props.urlDefault)) {
      this.setState({ source: this.props.urlDefault })
    } else {
      this.setState({ source: { uri: this.props.urlDefault }})
    }
  }

  _getLoadingComponent() {
    switch(this.props.loadingComponent) {
      case 'wave':
        return <WaveIndicator color='red' count={4} size={40} />
      case 'dot':
        return <DotIndicator color='red' count={4} size={4} />
      default: {
        return <ActivityIndicator size="large" color="red" />
      }
    }
  }

  render () {
    let loading = this._getLoadingComponent()
    if(this.state.source) {
      return (
        <FlexImage
          style={this.props.style}
          source={this.state.source}
          loadingComponent={loading}
          onPress={this.props.onPress}
        />
      )
    } else {
      return null
    }
  }
}

const styles = StyleSheet.create({ });

ImageFitLoading.propTypes = {
  name: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  path: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.string ]),
  urlBase: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  urlDefault: PropTypes.oneOfType([ PropTypes.object, PropTypes.string, PropTypes.number ]),
  urlNormalizer: PropTypes.func,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  token: PropTypes.string,
  loadingComponent: PropTypes.string,
  onPress: PropTypes.func,
}

ImageFitLoading.defaultProps = {
  name: 'Foto',
  urlNormalizer: getFirstParam,
  style: { flex: 1 },
  styleContainer: { flex: 1 },
  loadingComponent: 'default',
}
