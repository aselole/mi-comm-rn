import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'

export default class ProfileBox extends Component {
  static propTypes = {
    onChangePass: PropTypes.func.isRequired,
    nama: PropTypes.string,
    pusatStudi: PropTypes.string,
    email: PropTypes.string
  }

  render () {
    const { onChangePass, nama, pusatStudi, email } = this.props
    return (
      <View style={[ stylesTheme.itemWhiteBox, styles.panelMain ]}>
        <TouchableOpacity style={stylesTheme.editButton}
          onPress={onChangePass}>
          <Image
            source={require('../Assets/icPass.png')}
            style={{width: 45,height: 45}} />
        </TouchableOpacity>
        <View style={styles.panelHeader}>
          <Text style={[ stylesTheme.itemHeaderText, stylesTheme.whiteColor, stylesTheme.extraBold, ]}>
            Profil
          </Text>
        </View>
        <View style={styles.panelBody}>
          <View style={styles.infoField}>
            <Text style={[ stylesTheme.smallText, stylesTheme.greyColor, stylesTheme.regularBold, ]}>
              Nama
            </Text>
            <Text style={[stylesTheme.blackColor,styles.value]}>
              {nama}
            </Text>
          </View>
          <View>
            <View style={styles.infoField}>
              <Text style={[ stylesTheme.smallText, stylesTheme.greyColor, stylesTheme.regularBold, ]}>
                Pusat Studi
              </Text>
              <Text style={[stylesTheme.blackColor,styles.value]}>
                {pusatStudi}
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.infoField}>
              <Text style={[ stylesTheme.smallText, stylesTheme.greyColor, stylesTheme.regularBold, ]}>
                Email
              </Text>
              <Text style={[stylesTheme.blackColor,styles.value]}>
                {email}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  panelMain: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    marginBottom: 0
  },
  panelHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 15,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: 'rgb(150,150,150)'
  },
  panelBody: {
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  infoField: {
    width: 200,
  },
  value: {
    lineHeight: 23,
    fontSize: 15
  }
});
