import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import ConfirmDialog from './ConfirmDialog'

export default class MenuButton extends Component {

  constructor(props) {
    super(props)
    this.state = {
      confirm: 0,
    }
  }

  _onPress = () => {
    this.setState((prev) => { return {confirm:prev.confirm+1} })
  }

  render () {
    let onPress = this.props.isNeedConfirm ? this._onPress : this.props.onPress
    return (
      <View>
        <View>
          <TouchableOpacity style={styles.container} onPress={onPress}>
            <View style={styles.inner}>
              <Image source={this.props.image} style={styles.icon} resizeMode='contain'/>
              <Text style={styles.title}>{this.props.title}</Text>
            </View>
          </TouchableOpacity>
        </View>

        <ConfirmDialog
          isVisible={this.state.confirm}
          onPressOk={this.props.onPress}
          title={this.props.confirmTitle}
          message={this.props.confirmMessage}
          icon={this.props.confirmIcon} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 40,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inner: {
    flexDirection: 'row'
  },
  icon: {
    flex: 1,
    height: 25,
    width: 25
  },
  title: {
    flex: 3,
    alignSelf: 'center'
  }
});

MenuButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  image: PropTypes.number,
  title: PropTypes.string,
  isNeedConfirm: PropTypes.bool,
  confirmTitle: PropTypes.string,
  confirmMessage: PropTypes.string,
  confirmIcon: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
}

MenuButton.defaultProps = {
  isNeedConfirm: false,
}
