import { StyleSheet } from 'react-native'
import { Colors } from '../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  buttonContainer: {
    paddingLeft: vw * 3,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  button: {
    flexGrow: 0.5
  },
  headerLogin: {
    backgroundColor: Colors.transparent
  },
  header: {
		height: 23,
    backgroundColor: Colors.facebook
  },
  icon: {
    width: 23,
		height: 23,
  },
  tabs: {
    backgroundColor: Colors.silver
  },
	indicator: {
		width: 0,
    height: 0,
    backgroundColor: 'transparent'
	},
	label: {
		fontSize: 10
	},
})
