import React, { Component } from 'react'
import { View } from 'react-native'
import { StackNavigator, TabNavigator } from 'react-navigation'
import { withMappedNavigationProps } from 'react-navigation-props-mapper'

import Splash from '../Containers/Splash/Splash'
import Signin from '../Containers/User/Signin'
import Signup from '../Containers/User/Signup'
import Home from '../Containers/User/Home'
import Tentang from '../Containers/Dashboard/Tentang'
import Dashboard from '../Containers/Dashboard/Dashboard'
import PartnerList from '../Containers/Partner/PartnerList'
import PartnerDetail from '../Containers/Partner/PartnerDetail'
import DealList from '../Containers/Partner/DealList'
import DealDetail from '../Containers/Partner/DealDetail'
import BeritaList from '../Containers/Berita/BeritaList'
import BeritaDetail from '../Containers/Berita/BeritaDetail'
import TrainerList from '../Containers/Trainer/TrainerList'
import TrainerDetail from '../Containers/Trainer/TrainerDetail'
import InputKonsultasi from '../Containers/Trainer/InputKonsultasi'
import EventList from '../Containers/Event/EventList'
import EventDetail from '../Containers/Event/EventDetail'
import ObrolanList from '../Containers/Obrolan/ObrolanList'
import ObrolanDetail from '../Containers/Obrolan/ObrolanDetail'
import ObrolanChat from '../Containers/Obrolan/ObrolanChat'
import InputIssue from '../Containers/Obrolan/InputIssue'
import ProfilDetail from '../Containers/Profil/ProfilDetail'
import EditBio from '../Containers/Profil/EditBio'
import EditPassword from '../Containers/Profil/EditPassword'
import Peta from '../Containers/Peta/Peta'
import { Colors, Images, Strings } from '../Themes/'
import { ToolbarButton } from '../Components/'
import styles from './Style'
import css from '../Themes/Style'

export const PrimaryNav = StackNavigator({
    Splash: { screen: withMappedNavigationProps(Splash) },
    Home: { screen: withMappedNavigationProps(Home) },
    Signin: { screen: withMappedNavigationProps(Signin) },
    Signup: { screen: withMappedNavigationProps(Signup) },
    Tentang: { screen: withMappedNavigationProps(Tentang) },
    Dashboard: { screen: withMappedNavigationProps(Dashboard) },
    PartnerList: { screen: withMappedNavigationProps(PartnerList) },
    PartnerDetail: { screen: withMappedNavigationProps(PartnerDetail) },
    DealList: { screen: withMappedNavigationProps(DealList) },
    DealDetail: { screen: withMappedNavigationProps(DealDetail) },
    Peta: { screen: withMappedNavigationProps(Peta) },
    BeritaList: { screen: withMappedNavigationProps(BeritaList) },
    BeritaDetail: { screen: withMappedNavigationProps(BeritaDetail) },
    TrainerList: { screen: withMappedNavigationProps(TrainerList) },
    TrainerDetail: { screen: withMappedNavigationProps(TrainerDetail) },
    InputKonsultasi: { screen: withMappedNavigationProps(InputKonsultasi) },
    EventList: { screen: withMappedNavigationProps(EventList) },
    EventDetail: { screen: withMappedNavigationProps(EventDetail) },
    ObrolanList: { screen: withMappedNavigationProps(ObrolanList) },
    ObrolanDetail: { screen: withMappedNavigationProps(ObrolanDetail) },
    ObrolanChat: { screen: withMappedNavigationProps(ObrolanChat) },
    InputIssue: { screen: withMappedNavigationProps(InputIssue) },
    ProfilDetail: { screen: withMappedNavigationProps(ProfilDetail) },
    EditBio: { screen: withMappedNavigationProps(EditBio) },
    EditPassword: { screen: withMappedNavigationProps(EditPassword) },
  }, {
    initialRouteName: 'Splash',
    headerMode: 'screen',
    navigationOptions: ({ navigation }) => ({
      title: 'Mi-Comm',
      headerLeft:
        <ToolbarButton
          onPress={() => navigation.goBack()}
          image={Images.left_black}
          styleContainer={styles.buttonContainer}
          styleIcon={styles.button}
        />,
      headerTitleStyle: css.toolbarTitle,
      headerRight: <View></View>
    })
})

export default PrimaryNav
