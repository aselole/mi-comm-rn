var _ = require('lodash')

export function isStringEmpty(input) {
  return (input == null || typeof input === 'undefined' || !input || 0 === input.length)
}

export function isNull(input) {
  return (input == null || typeof input === 'undefined')
}

export function isNotNull(input) {
  return (input !== null && typeof input !== 'undefined')
}

export function isLocalImage(input) {
  return input.includes('file:///')
}

export function isAssetImage(input) {
  if(_.isNumber(input)) return true
  else return false
}

export function isServerImage(input) {
  return input.includes('http://')
}

export function isObject(input) {
  return (Object.prototype.toString.call(input) === '[object Object]')
}

export function isString(input) {
  return (Object.prototype.toString.call(input) === '[object String]')
}

export function isArray(input) {
  return (Object.prototype.toString.call(input) === '[object Array]')
}

export function getType(input) {
  return Object.prototype.toString.call(input)
}
