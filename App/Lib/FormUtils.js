import { getFilenameFromPath } from './FileUtils'
import { removeLastComma } from './ManipulationUtils'

// cari data bertipe teks dari tiap form
export function traverseTextValue(output, items) {
  if(typeof items === 'undefined') return output
  Object.keys(items).forEach((key) =>  {
      let keyType = Object.prototype.toString.call(items[key])
      if (keyType === '[object Object]') {
        if(items[key].hasOwnProperty('type'))
          if(items[key]['type'] === 'image') return   // tidak perlu masukkan data bertipe attachment
        return exports.traverseTextValue(output, items[key])
      }
      output[key] = items[key]
  })
}

// selaraskan semua data bertipe teks dari tiap form jadi satu object
export function getTextDataUpload(form, rawData) {
  let outputText = {}
  rawData.map((items) => {
    let temp = {}
    exports.traverseTextValue(temp, items)
    outputText = Object.assign({}, outputText, temp)
  })
  return outputText
}

// cari data bertipe attachment dari tiap form
export function traverseAttachmentValue(output, items) {
  if(typeof items === 'undefined') return output
  Object.keys(items).forEach((key) =>  {
      let keyType = Object.prototype.toString.call(items[key])
      if (keyType === '[object Object]') {
        if(items[key].hasOwnProperty('type')) {
          if(items[key]['type'] === 'image' && items[key].hasOwnProperty('data')) {
            output[key] = items[key]['data']    // masukkan data bertipe attachment saja
          }
        }
        return exports.traverseAttachmentValue(output, items[key])
      }
  })
}

// selaraskan semua data bertipe attachment dari tiap form jadi satu object
export function getAttachmentDataUpload(form, rawData) {
  let outputAttachment = {}
  rawData.map((items) => {
    let temp = {}
    exports.traverseAttachmentValue(temp, items)
    outputAttachment = Object.assign({}, outputAttachment, temp)
  })
  return outputAttachment
}

// data untuk update mapping nama attachment pada tabel
export function getAttachmentMapUpload(input, tabel) {
  let output = {}
  let columnName = ''
  Object.keys(input).forEach((key) =>  {
    columnName += key + ','

    let columnValue = ''
    if(input[key]) {
      input[key].map((item) => {
        columnValue += getFilenameFromPath(item) + ','
      })
    } else {
      columnValue = null
    }
    output[key] = removeLastComma(columnValue)
  })
  output['COLUMN_NAME'] = removeLastComma(columnName)
  output['TABEL'] = tabel
  return output
}
