var _ = require('lodash')

export function addFilterAll(json) {
  var data = [ {ID: "99", NAMA: "Semua"} ];
  data = data.concat(json);
  return data;
}

export function getEnumFromJson(json) {
  var output = {};
  for (let i = 0; i < json.result.length; i++) {
     row = json.result[i];
     output[ row.ID ] = row.NAMA;
  }
  return output;
}

export function replaceNull(key,value) {
  if (value === null || value == null || value === '') return undefined
  return value
}

export function replacer(key,value) {
  if (value === null || value == null) return undefined
  return value
}

export function jsonToFormEncoded(jsonObject) {
  var clone = JSON.parse(JSON.stringify(jsonObject))
  for(var prop in clone)
     if(clone[prop] == null)
         delete clone[prop];
  return Object.entries(clone).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&');
}

export function jsonToFormEncodedCaps(jsonObject) {
  if(_.isNil(jsonObject)) return
  var clone = JSON.parse(JSON.stringify(jsonObject))
  return Object.entries(clone).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&');
}
