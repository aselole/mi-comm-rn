import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  startup: ['startedup'],
})

export const StartupTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  startedup: false
})

/* ------------- Reducers ------------- */

export const startup = (state, {startedup}) => state.merge({ startedup })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP]: startup,
})
