import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  resetTransaction: null,
  onTransactionReceived: ['response'],
  onTransactionError: ['error_info'],
  takePromo: ['token', 'promo_id', 'kode'],
  editBio: ['token', 'nama', 'gender', 'phone', 'address'],
  editPass: ['token', 'password', 'confirm_password'],
  newComment: ['token', 'msg_loading', 'issue_id', 'comment', 'parent_id'],
  deleteComment: ['token', 'msg_loading', 'id'],
  reportComment: ['token', 'issue_comment_id'],
  newIssue: ['token', 'judul', 'body', 'featured_img'],
  deleteIssue: ['token', 'id'],
  newChatMessage: ['token', 'private_issue_id', 'comment'],
  newChatRoom: ['token', 'user_trainer_id', 'judul', 'body'],
})

export const TransactionTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  command: null,
  response: null,
  error_info: null,
  is_fetching: false,
  msg_loading: '',

  token: null,
  promo_id: null,
  kode: null,

  nama: '',
  gender: '',
  phone: '',
  address: '',

  issue_id: '',
  comment: '',
  parent_id: '',
})

/* ------------- Reducers ------------- */

export const resetTransaction = (state) => INITIAL_STATE

export const onTransactionReceived = (state, { response }) =>
  state.merge({ response, is_fetching: false })

export const onTransactionError = (state, { error_info }) =>
  state.merge({ error_info, is_fetching: false })

export const takePromo = (state, { token, promo_id, kode }) =>
  state.merge({
    token,
    promo_id,
    kode,
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const editBio = (state, { token, nama, gender, phone, address }) =>
  state.merge({
    token,
    nama, gender, phone, address,
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const editPass = (state, { token, password, confirm_password }) =>
  state.merge({
    token,
    password,
    confirm_password,
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const newComment = (state, { token, msg_loading, issue_id, comment, parent_id }) =>
  state.merge({
    token,
    issue_id, comment, parent_id,
    msg_loading,
    command: 'new_comment',
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const deleteComment = (state, { token, msg_loading, id }) =>
  state.merge({
    token, id,
    msg_loading,
    command: 'delete_comment',
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const reportComment = (state, { token, issue_comment_id }) =>
  state.merge({
    token,
    issue_comment_id,
    command: 'report_comment',
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const newIssue = (state, { token, judul, body, featured_img }) =>
  state.merge({
    token,
    judul, body, featured_img,
    msg_loading: 'Upload Issue Baru',
    command: 'new_issue',
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const deleteIssue = (state, { token, id }) =>
  state.merge({
    token, id,
    msg_loading: 'Menghapus Issue',
    command: 'delete_issue',
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const newChatMessage = (state, { token, private_issue_id, comment }) =>
  state.merge({
    token, private_issue_id,
    comment,
    is_fetching: true,
    response: null,
    error_info: null,
  })

export const newChatRoom = (state, { token, user_trainer_id, judul, body }) =>
  state.merge({
    token,
    user_trainer_id, judul, body,
    msg_loading: 'Membuka Obrolan Private Baru',
    is_fetching: true,
    response: null,
    error_info: null,
  })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_TRANSACTION]: resetTransaction,
  [Types.ON_TRANSACTION_RECEIVED]: onTransactionReceived,
  [Types.ON_TRANSACTION_ERROR]: onTransactionError,
  [Types.TAKE_PROMO]: takePromo,
  [Types.EDIT_BIO]: editBio,
  [Types.EDIT_PASS]: editPass,
  [Types.NEW_COMMENT]: newComment,
  [Types.DELETE_COMMENT]: deleteComment,
  [Types.REPORT_COMMENT]: reportComment,
  [Types.NEW_ISSUE]: newIssue,
  [Types.DELETE_ISSUE]: deleteIssue,
  [Types.NEW_CHAT_MESSAGE]: newChatMessage,
  [Types.NEW_CHAT_ROOM]: newChatRoom,
})
