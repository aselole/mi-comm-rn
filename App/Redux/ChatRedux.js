import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  reset: null,
  onMessageArrived: ['new_message'],
  appendMessage: ['messages'],
  onMessageRead: ['new_chat_room_read_id'],
  updateMessageRead: ['chat_room_read_id']
})

export const ChatTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  /*
  * nyimpan message yg baru datang berupa push notifikasi
  * stuktur data:
  *   [
        {id:1,
          [
            {title:'title1',body:'body1'},
            {title:'title2',body:'body2'}
          ]
        }
      ]
  */
  messages: [],
  new_message: [],
  new_chat_room_read_id: null,
  chat_room_read_id: [],
})

/* ------------- Reducers ------------- */

export const reset = ( state ) => INITIAL_STATE

export const onMessageArrived = ( state, { new_message }) =>
  state.merge({ new_message })

export const appendMessage = (state, { messages }) =>
  state.merge({ messages })

export const onMessageRead = ( state, { new_chat_room_read_id }) =>
  state.merge({ new_chat_room_read_id })

export const updateMessageRead = ( state, { chat_room_read_id }) =>
  state.merge({ chat_room_read_id })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET]: reset,
  [Types.ON_MESSAGE_ARRIVED]: onMessageArrived,
  [Types.APPEND_MESSAGE]: appendMessage,
  [Types.ON_MESSAGE_READ]: onMessageRead,
  [Types.UPDATE_MESSAGE_READ]: updateMessageRead,
})

/* ------------- Selector ------------- */

export const new_message = (state) => state.chat.new_message
export const messages = (state) => state.chat.messages
export const chat_room_read_id = (state) => state.chat.chat_room_read_id
