import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  onScreenChanged: ['active'],
  reset: null,
})

export const ScreenTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  active: null,
})

/* ------------- Reducers ------------- */

export const onScreenChanged = (state, { active }) =>
  state.merge({ active })

export const reset = (state) => INITIAL_STATE


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_SCREEN_CHANGED]: onScreenChanged,
  [Types.RESET]: reset,
})
