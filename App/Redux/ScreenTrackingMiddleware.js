import { NavigationActions } from 'react-navigation'
import ScreenAction from '../Redux/ScreenRedux'
import { store } from '../Containers/App'       // how to access redux from non-react component
import { Keyboard } from 'react-native'

// gets the current screen from navigation state
const getCurrentRouteName = (navigationState) => {
  if (!navigationState) {
    return null
  }
  const route = navigationState.routes[navigationState.index]
  // dive into nested navigators
  if (route.routes) {
    return getCurrentRouteName(route)
  }
  return route.routeName
}

const screenTracking = ({ getState }) => next => (action) => {
  if (
    action.type !== NavigationActions.NAVIGATE &&
    action.type !== NavigationActions.BACK
  ) {
    return next(action)
  }

  const currentScreen = getCurrentRouteName(getState().nav)
  const result = next(action)
  const nextScreen = getCurrentRouteName(getState().nav)
  if (nextScreen !== currentScreen) {
    try {
      Keyboard.dismiss()
      store.dispatch(ScreenAction.onScreenChanged(nextScreen))
    } catch (e) {
      alert(e)
    }
  }
  return result
}

export default screenTracking
