import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  signup: ['email', 'password', 'first_name'],
  signupOk: ['response_signup'],
  signupFail: ['error_info_signup'],
  signin: ['username', 'password', 'token_firebase'],
  signinOk: ['data_user'],
  signinFail: ['error_info_signin'],
  signout: null,
  forgotPass: ['username'],
  forgotPassOk: ['id_user'],
  forgotPassFail: ['error_info'],
  updatePass: ['id_user', 'password'],
  updatePassOk: null,
  updatePassFail: ['error_info'],
  updateBio: ['data_user'],
  updateFoto: ['foto_edit'],
  resetUser: null,
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching_signup: false,
  error_info_signup: null,
  response_signup: null,
  ok_signup: null,

  fetching_signin: false,
  error_info_signin: null,
  ok_signin: null,
  data_user: {},

  fetching: false,
  error_info: '',
  ok_forgot_pass: null,
  ok_update_pass: null,
  username: '',
  password: '',
  token_firebase: '',
  email: '',
  first_name: '',
  foto_edit: null,
})

/* ------------- Reducers ------------- */

export const signup = (state, { email, password, first_name }) =>
  state.merge({ fetching_signup: true, ok_signup: null, error_info_signup: null, email, password, first_name })

export const signupOk = (state, { response_signup }) =>
  state.merge({ fetching_signup: false, ok_signup: true, response_signup })

export const signupFail = (state, { error_info_signup }) =>
  state.merge({ fetching_signup: false, ok_signup: false, error_info_signup })

export const signin = (state, { username, password, token_firebase }) =>
  state.merge({ fetching_signin: true, ok_signin: null, error_info_signin: null, username, password, token_firebase })

export const signinOk = (state, { data_user }) =>
  state.merge({ fetching_signin: false, ok_signin: true, data_user })

export const signinFail = (state, { error_info_signin }) =>
  state.merge({ fetching_signin: false, ok_signin: false, data: null, username: null, password: null, error_info_signin })

export const signout = (state) => INITIAL_STATE

export const forgotPass = (state, { username }) =>
  state.merge({ fetching: true, ok_forgot_pass: null, error_info: '', username })

export const forgotPassOk = (state, { id_user }) =>
  state.merge({ fetching: false, ok_forgot_pass: true })

export const forgotPassFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_forgot_pass: false, error_info })


export const updatePass = (state, { id_user, password }) =>
  state.merge({ fetching: true, ok_update_pass: null, error_info: null, id_user, password })

export const updatePassOk = (state, { }) =>
  state.merge({ fetching: false, ok_update_pass: true })

export const updatePassFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_update_pass: false, error_info })


export const updateBio = (state, { data_user }) =>
  state.merge({ data_user })

export const updateFoto = (state, { foto_edit }) =>
  state.merge({ foto_edit })

export const resetUser = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGNIN]: signin,
  [Types.SIGNIN_OK]: signinOk,
  [Types.SIGNIN_FAIL]: signinFail,
  [Types.SIGNOUT]: signout,
  [Types.SIGNUP]: signup,
  [Types.SIGNUP_OK]: signupOk,
  [Types.SIGNUP_FAIL]: signupFail,
  [Types.FORGOT_PASS]: forgotPass,
  [Types.FORGOT_PASS_OK]: forgotPassOk,
  [Types.FORGOT_PASS_FAIL]: forgotPassFail,
  [Types.UPDATE_PASS]: updatePass,
  [Types.UPDATE_PASS_OK]: updatePassOk,
  [Types.UPDATE_PASS_FAIL]: updatePassFail,
  [Types.UPDATE_BIO]: updateBio,
  [Types.UPDATE_FOTO]: updateFoto,
  [Types.RESET_USER]: resetUser,
})
