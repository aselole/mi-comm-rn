import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    screen: require('./ScreenRedux').reducer,
    nav: require('./NavigationRedux').reducer,
    user: require('./UserRedux').reducer,
    startup: require('./StartupRedux').reducer,
    list: require('./ListRedux').reducer,
    transaction: require('./TransactionRedux').reducer,
    upload: require('./UploadRedux').reducer,
    chat: require('./ChatRedux').reducer,
  })

  return configureStore(rootReducer, rootSaga)
}
